USE [master]
GO
/****** Object:  Database [Restaurantv1.0]    Script Date: 2016-11-06 8:58:30 PM ******/
CREATE DATABASE [Restaurantv1.0]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Restaurant', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\Restaurant.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Restaurant_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\Restaurant_log.ldf' , SIZE = 2048KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [Restaurantv1.0] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Restaurantv1.0].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Restaurantv1.0] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Restaurantv1.0] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Restaurantv1.0] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Restaurantv1.0] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Restaurantv1.0] SET ARITHABORT OFF 
GO
ALTER DATABASE [Restaurantv1.0] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Restaurantv1.0] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Restaurantv1.0] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Restaurantv1.0] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Restaurantv1.0] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Restaurantv1.0] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Restaurantv1.0] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Restaurantv1.0] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Restaurantv1.0] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Restaurantv1.0] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Restaurantv1.0] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Restaurantv1.0] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Restaurantv1.0] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Restaurantv1.0] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Restaurantv1.0] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Restaurantv1.0] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Restaurantv1.0] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Restaurantv1.0] SET RECOVERY FULL 
GO
ALTER DATABASE [Restaurantv1.0] SET  MULTI_USER 
GO
ALTER DATABASE [Restaurantv1.0] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Restaurantv1.0] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Restaurantv1.0] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Restaurantv1.0] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [Restaurantv1.0] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'Restaurantv1.0', N'ON'
GO
USE [Restaurantv1.0]
GO
/****** Object:  Table [dbo].[Address]    Script Date: 2016-11-06 8:58:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Address](
	[AddressID] [int] IDENTITY(1,1) NOT NULL,
	[Street] [varchar](50) NULL,
	[City] [varchar](50) NULL,
	[PostalCode] [nchar](10) NULL,
	[Country] [varchar](50) NULL,
 CONSTRAINT [PK_Address] PRIMARY KEY CLUSTERED 
(
	[AddressID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Customer]    Script Date: 2016-11-06 8:58:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Customer](
	[CustomerID] [int] IDENTITY(1,1) NOT NULL,
	[Firstname] [varchar](50) NULL,
	[Lastname] [varchar](50) NULL,
	[Phone] [varchar](50) NULL,
	[Email] [varchar](50) NULL,
	[Version] [int] NULL CONSTRAINT [DF_Version_Customer]  DEFAULT ((1)),
	[TableNum] [int] NULL,
 CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED 
(
	[CustomerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CustomerAddress]    Script Date: 2016-11-06 8:58:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustomerAddress](
	[CustomerID] [int] NOT NULL,
	[AddressID] [int] NOT NULL,
 CONSTRAINT [PK_CustomerAddress] PRIMARY KEY CLUSTERED 
(
	[CustomerID] ASC,
	[AddressID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CustomerItem]    Script Date: 2016-11-06 8:58:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustomerItem](
	[CustomerItemID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerID] [int] NOT NULL,
	[ItemID] [int] NOT NULL,
	[Date] [datetime] NOT NULL CONSTRAINT [DF_Date_CustomerItem]  DEFAULT (getdate()),
	[IsComplete] [nchar](5) NULL,
 CONSTRAINT [PK_CustomerItem] PRIMARY KEY CLUSTERED 
(
	[CustomerItemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Employee]    Script Date: 2016-11-06 8:58:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Employee](
	[EmployeeID] [int] IDENTITY(1,1) NOT NULL,
	[Firstname] [varchar](50) NULL,
	[Lastname] [varchar](50) NULL,
	[Phone] [varchar](50) NULL,
	[Email] [varchar](50) NULL,
	[Version] [int] NULL CONSTRAINT [DF_Version_Employee]  DEFAULT ((1)),
	[PayRate] [money] NULL,
	[HoursWorked] [int] NULL CONSTRAINT [DF_HoursWorked_Employee]  DEFAULT ((0)),
 CONSTRAINT [PK_Employee] PRIMARY KEY CLUSTERED 
(
	[EmployeeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EmployeeAddress]    Script Date: 2016-11-06 8:58:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmployeeAddress](
	[EmployeeID] [int] NOT NULL,
	[AddressID] [int] NOT NULL,
 CONSTRAINT [PK_EmployeeAddress] PRIMARY KEY CLUSTERED 
(
	[EmployeeID] ASC,
	[AddressID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Item]    Script Date: 2016-11-06 8:58:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Item](
	[ItemID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[Price] [money] NULL,
	[Version] [int] NULL CONSTRAINT [DF_Version_Item]  DEFAULT ((1)),
 CONSTRAINT [PK_Item] PRIMARY KEY CLUSTERED 
(
	[ItemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[CustomerBill]    Script Date: 2016-11-06 8:58:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[CustomerBill]
AS
SELECT cust.CustomerID,
		cust.Firstname,
		cust.Lastname,
		cust.Phone,
		cust.Email,
		cust.Version AS 'Customer Version',
		itm.ItemID,
		itm.Name,
		itm.Price,
		itm.Version AS 'Item Version'
FROM dbo.Customer cust
INNER JOIN dbo.CustomerItem
ON cust.CustomerID = dbo.CustomerItem.CustomerID
INNER JOIN dbo.Item itm
ON dbo.CustomerItem.ItemID = itm.ItemID
WHERE IsComplete = 'False'

GO
/****** Object:  View [dbo].[CustomerHistory]    Script Date: 2016-11-06 8:58:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [dbo].[CustomerHistory]
AS
SELECT Customer.CustomerID,Customer.Firstname,Customer.Lastname,Item.ItemID,Item.Name,Item.Price 
FROM Customer
INNER JOIN dbo.CustomerItem
ON Customer.CustomerID = CustomerItem.CustomerID
INNER JOIN Item
ON CustomerItem.ItemID = Item.ItemID
WHERE IsComplete = 'True'


GO
/****** Object:  View [dbo].[FullCustomer]    Script Date: 2016-11-06 8:58:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[FullCustomer]
AS
SELECT dbo.Customer.CustomerID,
		dbo.Customer.Firstname,
		dbo.Customer.Lastname,
		dbo.Customer.Phone,
		dbo.Customer.Email,
		dbo.Customer.Version,
		dbo.Address.AddressID,
		dbo.Address.Street,
		dbo.Address.City,
		dbo.Address.PostalCode,
		dbo.Address.Country
		FROM 
		dbo.Customer 
		INNER JOIN dbo.CustomerAddress
		ON dbo.Customer.CustomerID = dbo.CustomerAddress.CustomerID
		INNER JOIN dbo.Address
		ON dbo.CustomerAddress.AddressID = dbo.Address.AddressID;



GO
/****** Object:  View [dbo].[FullCustomerIncomplete]    Script Date: 2016-11-06 8:58:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE VIEW [dbo].[FullCustomerIncomplete]
AS
SELECT DISTINCT (dbo.Customer.CustomerID),
		dbo.Customer.Firstname,
		dbo.Customer.Lastname,
		dbo.Customer.Phone,
		dbo.Customer.Email,
		dbo.Customer.Version,
		dbo.Address.AddressID,
		dbo.Address.Street,
		dbo.Address.City,
		dbo.Address.PostalCode,
		dbo.Address.Country
		FROM 
		dbo.Customer 
		INNER JOIN dbo.CustomerAddress
		ON dbo.Customer.CustomerID = dbo.CustomerAddress.CustomerID
		INNER JOIN dbo.Address
		ON dbo.CustomerAddress.AddressID = dbo.Address.AddressID
		INNER JOIN dbo.CustomerItem
		ON dbo.Customer.CustomerID = CustomerItem.CustomerID
		WHERE CustomerItem.IsComplete = 'False'


GO
/****** Object:  View [dbo].[FullEmployee]    Script Date: 2016-11-06 8:58:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [dbo].[FullEmployee]
AS
SELECT dbo.Employee.EmployeeID,
		dbo.Employee.Firstname,
		dbo.Employee.Lastname,
		dbo.Employee.Phone,
		dbo.Employee.Email,
		dbo.Employee.Version,
		dbo.Employee.PayRate,
		dbo.Employee.HoursWorked,
		dbo.Address.AddressID,
		dbo.Address.Street,
		dbo.Address.City,
		dbo.Address.PostalCode,
		dbo.Address.Country
		FROM 
		dbo.Employee 
		INNER JOIN dbo.EmployeeAddress
		ON dbo.Employee.EmployeeID = dbo.EmployeeAddress.EmployeeID
		INNER JOIN dbo.Address
		ON dbo.EmployeeAddress.AddressID = dbo.Address.AddressID;





GO
ALTER TABLE [dbo].[CustomerAddress]  WITH CHECK ADD  CONSTRAINT [FK_CustomerAddress_Address] FOREIGN KEY([AddressID])
REFERENCES [dbo].[Address] ([AddressID])
GO
ALTER TABLE [dbo].[CustomerAddress] CHECK CONSTRAINT [FK_CustomerAddress_Address]
GO
ALTER TABLE [dbo].[CustomerAddress]  WITH CHECK ADD  CONSTRAINT [FK_CustomerAddress_Customer] FOREIGN KEY([CustomerID])
REFERENCES [dbo].[Customer] ([CustomerID])
GO
ALTER TABLE [dbo].[CustomerAddress] CHECK CONSTRAINT [FK_CustomerAddress_Customer]
GO
ALTER TABLE [dbo].[CustomerItem]  WITH CHECK ADD  CONSTRAINT [FK_CustomerItem_Customer] FOREIGN KEY([CustomerID])
REFERENCES [dbo].[Customer] ([CustomerID])
GO
ALTER TABLE [dbo].[CustomerItem] CHECK CONSTRAINT [FK_CustomerItem_Customer]
GO
ALTER TABLE [dbo].[CustomerItem]  WITH CHECK ADD  CONSTRAINT [FK_CustomerItem_Item] FOREIGN KEY([ItemID])
REFERENCES [dbo].[Item] ([ItemID])
GO
ALTER TABLE [dbo].[CustomerItem] CHECK CONSTRAINT [FK_CustomerItem_Item]
GO
ALTER TABLE [dbo].[EmployeeAddress]  WITH CHECK ADD  CONSTRAINT [FK_EmployeeAddress_Address] FOREIGN KEY([AddressID])
REFERENCES [dbo].[Address] ([AddressID])
GO
ALTER TABLE [dbo].[EmployeeAddress] CHECK CONSTRAINT [FK_EmployeeAddress_Address]
GO
ALTER TABLE [dbo].[EmployeeAddress]  WITH CHECK ADD  CONSTRAINT [FK_EmployeeAddress_Employee] FOREIGN KEY([EmployeeID])
REFERENCES [dbo].[Employee] ([EmployeeID])
GO
ALTER TABLE [dbo].[EmployeeAddress] CHECK CONSTRAINT [FK_EmployeeAddress_Employee]
GO
/****** Object:  StoredProcedure [dbo].[Address_Insert]    Script Date: 2016-11-06 8:58:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[Address_Insert](
							@street VARCHAR(50),
							@city VARCHAR(50),
							@pc NCHAR(10),
							@country VARCHAR(50),
							@id INT = 0 OUTPUT)
AS
INSERT INTO dbo.Address (Street,City,PostalCode,Country)
VALUES (@street,@city,@pc,@country)
SET @id = SCOPE_IDENTITY();


GO
/****** Object:  StoredProcedure [dbo].[Address_Update]    Script Date: 2016-11-06 8:58:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[Address_Update](
							@street VARCHAR(50),
							@city VARCHAR(50),
							@pc NCHAR(10),
							@country VARCHAR(50),
							@id INT)
AS
UPDATE dbo.Address 
SET Street = @street,City = @city,PostalCode = @pc,Country = @country
WHERE AddressID = @id;

GO
/****** Object:  StoredProcedure [dbo].[Customer_Add_Item]    Script Date: 2016-11-06 8:58:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE PROC [dbo].[Customer_Add_Item] (
								@custid INT,
								@itemid INT							
								)
AS
INSERT INTO dbo.CustomerItem(CustomerID,ItemID,IsComplete)
VALUES (@custid,@itemid,'True')



GO
/****** Object:  StoredProcedure [dbo].[Customer_Add_ItemIncomplete]    Script Date: 2016-11-06 8:58:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE PROC [dbo].[Customer_Add_ItemIncomplete] (
								@custid INT,
								@itemid INT							
								)
AS
INSERT INTO dbo.CustomerItem(CustomerID,ItemID,IsComplete)
VALUES (@custid,@itemid,'False')



GO
/****** Object:  StoredProcedure [dbo].[Customer_Delete]    Script Date: 2016-11-06 8:58:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE PROC [dbo].[Customer_Delete](@id INT)
AS

DELETE FROM dbo.CustomerAddress
WHERE CustomerID =  @id;

DELETE FROM dbo.Customer
WHERE CustomerID = @id





GO
/****** Object:  StoredProcedure [dbo].[Customer_FullInsert]    Script Date: 2016-11-06 8:58:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[Customer_FullInsert](
						@fname VARCHAR(50),
						@lname VARCHAR(50),
						@phone VARCHAR(50),
						@email VARCHAR(50),
						@street VARCHAR(50),
						@city VARCHAR(50),
						@pc VARCHAR(50),
						@country VARCHAR(50),
						@id int = 0 OUTPUT)
AS
INSERT INTO dbo.Customer(
	dbo.Customer.Firstname,
	dbo.Customer.Lastname,
	dbo.Customer.Phone,
	dbo.Customer.Email)
VALUES(	@fname,
		@lname,
		@phone,
		@email);
SET @id = SCOPE_IDENTITY();

INSERT INTO dbo.Address(
	dbo.address.Street,
	dbo.address.City,
	dbo.address.PostalCode,
	dbo.address.Country)
VALUES(	@street,
		@city,
		@pc,
		@country);

INSERT INTO dbo.CustomerAddress(CustomerID,AddressID)
VALUES (@id,SCOPE_IDENTITY());



GO
/****** Object:  StoredProcedure [dbo].[Customer_GetAll]    Script Date: 2016-11-06 8:58:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[Customer_GetAll]
AS
SELECT dbo.Customer.CustomerID,
		dbo.Customer.Firstname,
		dbo.Customer.Lastname,
		dbo.Customer.Phone,
		dbo.Customer.Email,
		dbo.Customer.Version,
		dbo.Customer.TableNum
FROM dbo.Customer



GO
/****** Object:  StoredProcedure [dbo].[Customer_GetAllFull]    Script Date: 2016-11-06 8:58:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROC [dbo].[Customer_GetAllFull]
AS
SELECT CustomerID,
		Firstname,
		Lastname,
		Phone,
		Email,
		Version,
		AddressID,
		Street,
		City,
		PostalCode,
		Country
		FROM 
		FullCustomer



GO
/****** Object:  StoredProcedure [dbo].[Customer_GetAllFullIncomplete]    Script Date: 2016-11-06 8:58:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROC [dbo].[Customer_GetAllFullIncomplete]
AS
SELECT CustomerID,
		Firstname,
		Lastname,
		Phone,
		Email,
		Version,
		AddressID,
		Street,
		City,
		PostalCode,
		Country
		FROM 
		FullCustomerIncomplete


GO
/****** Object:  StoredProcedure [dbo].[Customer_GetBill]    Script Date: 2016-11-06 8:58:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[Customer_GetBill](@id int)
AS
SELECT	ItemID,
		Name,
		Price
		FROM 
		CustomerBill

WHERE CustomerID = @id;

GO
/****** Object:  StoredProcedure [dbo].[Customer_GetById]    Script Date: 2016-11-06 8:58:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[Customer_GetById](@id int)
AS
SELECT	dbo.Customer.CustomerID,
		dbo.Customer.Firstname,
		dbo.Customer.Lastname,
		dbo.Customer.Phone,
		dbo.Customer.Email,
		dbo.Customer.Version,
		dbo.Customer.TableNum
FROM dbo.Customer
WHERE CustomerID = @id;




GO
/****** Object:  StoredProcedure [dbo].[Customer_GetByIdFull]    Script Date: 2016-11-06 8:58:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[Customer_GetByIdFull](@id INT)
AS
SELECT CustomerID,
		Firstname,
		Lastname,
		Phone,
		Email,
		Version,
		AddressID,
		Street,
		City,
		PostalCode,
		Country
		FROM 
		FullCustomer
		WHERE 
		CustomerID = @id;


GO
/****** Object:  StoredProcedure [dbo].[Customer_GetByIdFullIncomplete]    Script Date: 2016-11-06 8:58:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROC [dbo].[Customer_GetByIdFullIncomplete](@id INT)
AS
SELECT CustomerID,
		Firstname,
		Lastname,
		Phone,
		Email,
		Version,
		AddressID,
		Street,
		City,
		PostalCode,
		Country
		FROM 
		FullCustomerIncomplete
		WHERE 
		CustomerID = @id;


GO
/****** Object:  StoredProcedure [dbo].[Customer_GetTotal]    Script Date: 2016-11-06 8:58:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[Customer_GetTotal] (@id INT)
AS
SELECT SUM(Price) AS 'Total' from CustomerHistory WHERE CustomerID = @id


GO
/****** Object:  StoredProcedure [dbo].[Customer_Insert]    Script Date: 2016-11-06 8:58:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[Customer_Insert](
						@fname VARCHAR(50),
						@lname VARCHAR(50),
						@phone VARCHAR(50),
						@email VARCHAR(50),
						@tablenum INT,
						@id int = 0 OUTPUT)
AS
INSERT INTO dbo.Customer(
	dbo.Customer.Firstname,
	dbo.Customer.Lastname,
	dbo.Customer.Phone,
	dbo.Customer.Email,
	dbo.Customer.TableNum)
VALUES(	@fname,
		@lname,
		@phone,
		@email,
		@tablenum)
SET @id = SCOPE_IDENTITY()



GO
/****** Object:  StoredProcedure [dbo].[Customer_PayBill]    Script Date: 2016-11-06 8:58:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[Customer_PayBill](@id int)
AS
UPDATE dbo.CustomerItem
SET IsComplete = 'True'
WHERE CustomerID = @id;


GO
/****** Object:  StoredProcedure [dbo].[Customer_Update]    Script Date: 2016-11-06 8:58:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[Customer_Update](
							@fname	VARCHAR(50),
							@lname VARCHAR(50),
							@phone VARCHAR(50),
							@email VARCHAR(50),
							@tablenum INT,
							@id	int)
AS
UPDATE dbo.Customer
SET Firstname = @fname,
	Lastname = @lname, 
	Phone = @phone, 
	Email = @email,
	TableNum = @tablenum,
	Version += 1
WHERE  CustomerId = @id



GO
/****** Object:  StoredProcedure [dbo].[Customer_UpdateFull]    Script Date: 2016-11-06 8:58:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROC [dbo].[Customer_UpdateFull](
							@fname VARCHAR(50),
							@lname VARCHAR(50),
							@phone VARCHAR(50),
							@email VARCHAR(50),
							@street VARCHAR(50),
							@city VARCHAR(50),
							@pc VARCHAR(50),
							@country VARCHAR(50),
							@id	int)
AS
UPDATE dbo.Customer
SET Firstname = @fname,
	Lastname = @lname, 
	Phone = @phone, 
	Email = @email,
	Version += 1
WHERE  CustomerID = @id

UPDATE dbo.Address 
SET Street = @street,
	City = @city,
	PostalCode = @pc,
	Country = @country
FROM dbo.Address 
INNER JOIN dbo.CustomerAddress
ON Address.AddressID = CustomerAddress.AddressID
WHERE CustomerAddress.CustomerID = @id;



GO
/****** Object:  StoredProcedure [dbo].[Employee_Delete]    Script Date: 2016-11-06 8:58:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[Employee_Delete](@id int)
AS
DELETE FROM dbo.Employee
WHERE dbo.Employee.EmployeeID = @id;



GO
/****** Object:  StoredProcedure [dbo].[Employee_FullDelete]    Script Date: 2016-11-06 8:58:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[Employee_FullDelete](@id INT)
AS

DELETE FROM dbo.EmployeeAddress
WHERE EmployeeID =  @id;

DELETE FROM dbo.Employee
WHERE EmployeeID = @id



GO
/****** Object:  StoredProcedure [dbo].[Employee_FullInsert]    Script Date: 2016-11-06 8:58:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[Employee_FullInsert](
						@fname VARCHAR(50),
						@lname VARCHAR(50),
						@phone VARCHAR(50),
						@email VARCHAR(50),
						@pr MONEY,
						@street VARCHAR(50),
						@city VARCHAR(50),
						@pc VARCHAR(50),
						@country VARCHAR(50),
						@id int = 0 OUTPUT)
AS
INSERT INTO dbo.Employee(
	dbo.Employee.Firstname,
	dbo.Employee.Lastname,
	dbo.Employee.Phone,
	dbo.Employee.Email,
	dbo.Employee.PayRate)
VALUES(	@fname,
		@lname,
		@phone,
		@email,
		@pr);
SET @id = SCOPE_IDENTITY();

INSERT INTO dbo.Address(
	dbo.address.Street,
	dbo.address.City,
	dbo.address.PostalCode,
	dbo.address.Country)
VALUES(	@street,
		@city,
		@pc,
		@country);

INSERT INTO dbo.EmployeeAddress(EmployeeID,AddressID)
VALUES (@id,SCOPE_IDENTITY());



GO
/****** Object:  StoredProcedure [dbo].[Employee_GetAll]    Script Date: 2016-11-06 8:58:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[Employee_GetAll]
AS
SELECT EmployeeID,
		Firstname,
		Lastname,
		Phone,
		Email,
		Version,
		PayRate,
		HoursWorked,
		AddressID,
		Street,
		City,
		PostalCode,
		Country
		FROM 
		FullEmployee


GO
/****** Object:  StoredProcedure [dbo].[Employee_GetById]    Script Date: 2016-11-06 8:58:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROC [dbo].[Employee_GetById](@id INT)
AS
SELECT EmployeeID,
		Firstname,
		Lastname,
		Phone,
		Email,
		Version,
		PayRate,
		HoursWorked,
		AddressID,
		Street,
		City,
		PostalCode,
		Country
		FROM 
		FullEmployee
		WHERE 
		EmployeeID = @id;

GO
/****** Object:  StoredProcedure [dbo].[Employee_Insert]    Script Date: 2016-11-06 8:58:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[Employee_Insert](
						@fname VARCHAR(50),
						@lname VARCHAR(50),
						@phone VARCHAR(50),
						@email VARCHAR(50),
						@payrate MONEY,
						@id int = 0 OUTPUT)
AS
INSERT INTO dbo.Employee(
	dbo.Employee.Firstname,
	dbo.Employee.Lastname,
	dbo.Employee.Phone,
	dbo.Employee.Email,
	dbo.Empoyee.PayRate)
VALUES(	@fname,
		@lname,
		@phone,
		@email,
		@payrate);

SET @id = SCOPE_IDENTITY();



GO
/****** Object:  StoredProcedure [dbo].[Employee_Update]    Script Date: 2016-11-06 8:58:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[Employee_Update](
							@fname VARCHAR(50),
							@lname VARCHAR(50),
							@phone VARCHAR(50),
							@email VARCHAR(50),
							@pr MONEY,
							@hr INT,
							@street VARCHAR(50),
							@city VARCHAR(50),
							@pc VARCHAR(50),
							@country VARCHAR(50),
							@id	int)
AS
UPDATE dbo.Employee
SET Firstname = @fname,
	Lastname = @lname, 
	Phone = @phone, 
	Email = @email,
	PayRate = @pr,
	HoursWorked = @hr,
	Version += 1
WHERE  EmployeeID = @id

UPDATE dbo.Address 
SET Street = @street,
	City = @city,
	PostalCode = @pc,
	Country = @country
FROM dbo.Address 
INNER JOIN dbo.EmployeeAddress
ON Address.AddressID = EmployeeAddress.AddressID
WHERE EmployeeAddress.EmployeeID = @id;


GO
/****** Object:  StoredProcedure [dbo].[Item_Delete]    Script Date: 2016-11-06 8:58:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[Item_Delete](
						@id INT)
AS
DELETE FROM dbo.Item 
WHERE ItemID = @id

GO
/****** Object:  StoredProcedure [dbo].[Item_GetAll]    Script Date: 2016-11-06 8:58:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[Item_GetAll]
AS
SELECT ItemID,Name,Price,Version
FROM Item

GO
/****** Object:  StoredProcedure [dbo].[Item_GetById]    Script Date: 2016-11-06 8:58:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[Item_GetById](@id INT)
AS
SELECT Name,Price,Version
FROM dbo.Item
WHERE ItemID = @id

GO
/****** Object:  StoredProcedure [dbo].[Item_Insert]    Script Date: 2016-11-06 8:58:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[Item_Insert](
						@name VARCHAR(50),
						@price MONEY,
						@id INT = 0 OUTPUT)
AS
INSERT INTO dbo.Item (Name,Price)
VALUES (@name,@price)
SET @id = SCOPE_IDENTITY()

GO
/****** Object:  StoredProcedure [dbo].[Item_Update]    Script Date: 2016-11-06 8:58:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROC [dbo].[Item_Update](
							@name	VARCHAR(50),
							@price MONEY,
							@id	INT)
AS
UPDATE dbo.Item
SET Name = @name,
	Price = @price, 
	Version += 1
WHERE  ItemId = @id



GO
USE [master]
GO
ALTER DATABASE [Restaurantv1.0] SET  READ_WRITE 
GO
