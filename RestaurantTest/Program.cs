﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestaurantVML;
using RestaurantDAL;

namespace RestaurantTest
{
    class Program
    {
        static void Main(string[] args)
        {
            CustomerVM cust = new CustomerVM();
            cust.Bill.Add(new ItemVM() { Name = "foo", Price = 3.00 });
            cust.Bill.Add(new ItemVM() { Name = "Bar", Price = 6.75 });
            Console.WriteLine(cust.Receipt());
        }
    }
}
