﻿///**************************************************************************************************
/// file:	ItemDAO.cs
/// <author>Joshua Wright</author>
/// <date>2016-11-06</date>
///*************************************************************************************************
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System;

namespace RestaurantDAL
{
    ///**************************************************************************************************
    ///<summary> An item dao.</summary>
    ///<remarks> Joshua Wright, 2016-11-06.</remarks>
    ///*************************************************************************************************
    public class ItemDAO
    {
        private DBContex dao_;
        private SqlCommand cmd;


        public ItemDAO()
        {
            dao_ = new DBContex();
            cmd = new SqlCommand();
            cmd.Connection = dao_.Con;
        }

        ///**************************************************************************************************
        ///<summary> Creates a new Item.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<param name="item"> The item.</param>
        ///<returns> An Item.</returns>
        ///*************************************************************************************************
        public Item Create(Item item)
        {
            cmd.CommandText = "dbo.Item_Insert";
            cmd.CommandType = CommandType.StoredProcedure;

            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@name", item.Name));
            parameters.Add(new SqlParameter("@price", item.Price));

            cmd.Parameters.AddRange(parameters.ToArray());
            try
            {
                item.Id = cmd.ExecuteNonQuery();
            }
            catch(SqlException ex)
            {
                Console.WriteLine("Error in ItemDAO, Create  " + ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error in ItemDAO, Create  " + ex.Message);
            }
            cmd.Parameters.Clear();
            item.Version = 1;
            return item;
        }

        ///**************************************************************************************************
        ///<summary> Deletes Item given ID.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<param name="id"> The Identifier to delete.</param>
        ///<returns> An int.</returns>
        ///*************************************************************************************************
        public int Delete(int id)
        {
            cmd.CommandText = "dbo.Item_Delete";
            cmd.CommandType = CommandType.StoredProcedure;       
            cmd.Parameters.Add(new SqlParameter("@id", id));
            int result = 0;
            try
            {
                result = cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();
            }
            catch (SqlException ex)
            {
                Console.WriteLine("Error in ItemDAO, Delete  " + ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error in ItemDAO, Delete  " + ex.Message);
            }
            return result;
        }

        ///**************************************************************************************************
        ///<summary> Gets Item by identifier.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<param name="id"> The Identifier to delete.</param>
        ///<returns> The by identifier.</returns>
        ///*************************************************************************************************
        public Item GetById(int id)
        {
            cmd.CommandText = "dbo.Item_GetById";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@id", id));

            Item item = new Item();

            try
            {
                SqlDataReader results = cmd.ExecuteReader();
                cmd.Parameters.Clear();

                while (results.Read())
                {
                    item.Name = results.GetString(0);
                    item.Price = results.GetSqlMoney(1).ToDouble();
                    item.Version = results.GetInt32(2);
                }
                results.Close();
            }
            catch (SqlException ex)
            {
                Console.WriteLine("Error in ItemDAO, GetById  " + ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error in ItemDAO, GetById  " + ex.Message);
            }
            return item;
        }

        ///**************************************************************************************************
        ///<summary> Gets all Items.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<returns> all.</returns>
        ///*************************************************************************************************
        public List<Item> GetAll()
        {
            cmd.CommandText = "dbo.Item_GetAll";
            cmd.CommandType = CommandType.StoredProcedure;
            List<Item> list = new List<Item>();

            try
            {
                SqlDataReader results = cmd.ExecuteReader();

                while (results.Read())
                {
                    Item item = new Item();
                    item.Id = results.GetInt32(0);
                    item.Name = results.GetString(1);
                    item.Price = results.GetSqlMoney(2).ToDouble();
                    item.Version = results.GetInt32(0);

                    list.Add(item);
                }
                results.Close();
            }
            catch (SqlException ex)
            {
                Console.WriteLine("Error in ItemDAO, GetAll  " + ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error in ItemDAO, GetAll  " + ex.Message);
            }

            return list;
        }

        ///**************************************************************************************************
        ///<summary> Updates the given item.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<param name="item"> The item.</param>
        ///<returns> An Item.</returns>
        ///*************************************************************************************************
        public Item Update(Item item)
        {
            cmd.CommandText = "dbo.Item_Update";
            cmd.CommandType = CommandType.StoredProcedure;

            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@name", item.Name));
            parameters.Add(new SqlParameter("@price", item.Price));
            parameters.Add(new SqlParameter("@id", item.Id));

            cmd.Parameters.AddRange(parameters.ToArray());

            try
            {
                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();
            }
            catch (SqlException ex)
            {
                Console.WriteLine("Error in ItemDAO, GetUpdate  " + ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error in ItemDAO, GetUpdate  " + ex.Message);
            }

            item.Version++;
            return item;
        }
    }
}
