﻿///**************************************************************************************************
/// file:	Customer.cs
/// <author>Joshua Wright</author>
/// <date>2016-11-06</date>
///*************************************************************************************************
namespace RestaurantDAL
{
    ///**************************************************************************************************
    ///<summary> A customer Entity.</summary>
    ///<remarks> Joshua Wright, 2016-11-06.</remarks>
    ///*************************************************************************************************
    public class Customer
    {
        public int Id { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public int Version { get; set; }
        public int TableNum { get; set; }
        public int AddressId { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
     
        public Customer(string fname = "unknown", string lname = "unknown"
            , string phone = "unknown", string email = "unknown",
            string steet = "unknown", string city = "unknown",
            string pc = "unknown", string country = "unknown")
        {
            Firstname = fname;
            Lastname = lname;
            Phone = phone;
            Email = email;
            Street = steet;
            City = city;
            PostalCode = pc;
            Country = country;
        }
    }
}
