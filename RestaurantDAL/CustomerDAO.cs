﻿///**************************************************************************************************
/// file:	CustomerDAO.cs
/// <author>Joshua Wright</author>
/// <date>2016-11-06</date>
///*************************************************************************************************
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;

namespace RestaurantDAL
{
    ///**************************************************************************************************
    ///<summary> A customer dao.</summary>
    ///<remarks> Joshua Wright, 2016-11-06.</remarks>
    ///*************************************************************************************************
    public class CustomerDAO
    {
        private DBContex dao_;
        private SqlCommand cmd;

        public CustomerDAO()
        {
            dao_ = new DBContex();
            cmd = new SqlCommand();
            cmd.Connection = dao_.Con;
        }

        ///**************************************************************************************************
        ///<summary> Creates a new Customer.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<param name="cust"> .</param>
        ///<returns>Customer.</returns>
        ///*************************************************************************************************
        public Customer Create(Customer cust)
        {
            cmd.CommandText = "dbo.Customer_Insert";
            cmd.CommandType = CommandType.StoredProcedure;

            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@fname", cust.Firstname));
            parameters.Add(new SqlParameter("@lname", cust.Lastname));
            parameters.Add(new SqlParameter("@phone", cust.Phone));
            parameters.Add(new SqlParameter("@email", cust.Email));
            parameters.Add(new SqlParameter("@tablenum", cust.TableNum));
            parameters.Add(new SqlParameter()
            {
                Direction = ParameterDirection.Output,
                ParameterName = "@id",
                SqlDbType = SqlDbType.Int
            });
            cmd.Parameters.AddRange(parameters.ToArray());

            try
            {
                cmd.ExecuteNonQuery();
                cust.Id = Convert.ToInt32(cmd.Parameters["@id"].Value);
                cmd.Parameters.Clear();
            }
            catch (SqlException ex)
            {
                Console.WriteLine("Error in CustomerDAO, Create  " + ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error in CustomerDAO, Create  " + ex.Message);
            }
            return cust;
        }

        ///**************************************************************************************************
        ///<summary> Creates a full Customer.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<param name="cust"> .</param>
        ///<returns>Customer</returns>
        ///*************************************************************************************************
        public Customer CreateFull(Customer cust)
        {
            cmd.CommandText = "dbo.Customer_FullInsert";
            cmd.CommandType = CommandType.StoredProcedure;

            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@fname", cust.Firstname));
            parameters.Add(new SqlParameter("@lname", cust.Lastname));
            parameters.Add(new SqlParameter("@phone", cust.Phone));
            parameters.Add(new SqlParameter("@email", cust.Email));
            parameters.Add(new SqlParameter("@street", cust.Street));
            parameters.Add(new SqlParameter("@city", cust.City));
            parameters.Add(new SqlParameter("@pc", cust.PostalCode));
            parameters.Add(new SqlParameter("@country", cust.Country));
            parameters.Add(new SqlParameter()
            {
                Direction = ParameterDirection.Output,
                ParameterName = "@id",
                SqlDbType = SqlDbType.Int              
            });          
            cmd.Parameters.AddRange(parameters.ToArray());

            try
            {
                cmd.ExecuteNonQuery();
                cust.Id = Convert.ToInt32(cmd.Parameters["@id"].Value);
                cmd.Parameters.Clear();
            }
            catch (SqlException ex)
            {
                Console.WriteLine("Error in CustomerDAO, CreateFull  " + ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error in CustomerDAO, CreateFull  " + ex.Message);
            }
            cust.Version = 1;

            return cust;
        }

        ///**************************************************************************************************
        ///<summary> Gets Customer by identifier.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<param name="id"> .</param>
        ///<returns> The by identifier.</returns>
        ///*************************************************************************************************
        public Customer GetById(int id)
        {
            cmd.CommandText = "dbo.Customer_GetById";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@id", id));

            Customer cust = new Customer();

            try
            {
                SqlDataReader results = cmd.ExecuteReader();
                cmd.Parameters.Clear();

                while (results.Read())
                {
                    cust.Id = results.GetInt32(0);
                    cust.Firstname = results.GetString(1);
                    cust.Lastname = results.GetString(2);
                    cust.Phone = results.GetString(3);
                    cust.Email = results.GetString(4);
                    cust.Version = results.GetInt32(5);
                    cust.TableNum = results.GetInt32(6);
                }
                results.Close();
            }
            catch (SqlException ex)
            {
                Console.WriteLine("Error in CustomerDAO, GetById  " + ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error in CustomerDAO, GetById  " + ex.Message);
            }
            return cust;
        }

        ///**************************************************************************************************
        ///<summary> Gets A full Customer by identifier.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<param name="id"> .</param>
        ///<returns> The by identifier.</returns>
        ///*************************************************************************************************
        public Customer GetByIdFull(int id)
        {
            cmd.CommandText = "dbo.Customer_GetByIdFull";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@id", id));

            Customer cust = new Customer();

            try
            {
                SqlDataReader results = cmd.ExecuteReader();
                cmd.Parameters.Clear();

                while (results.Read())
                {
                    cust.Id = results.GetInt32(0);
                    cust.Firstname = results.GetString(1);
                    cust.Lastname = results.GetString(2);
                    cust.Phone = results.GetString(3);
                    cust.Email = results.GetString(4);
                    cust.Version = results.GetInt32(5);
                    cust.AddressId = results.GetInt32(6);
                    cust.Street = results.GetString(7);
                    cust.City = results.GetString(8);
                    cust.PostalCode = results.GetString(9);
                    cust.Country = results.GetString(10);
                }
                results.Close();
            }
            catch (SqlException ex)
            {
                Console.WriteLine("Error in CustomerDAO, GetByIdFull  " + ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error in CustomerDAO, GetByIdFull  " + ex.Message);
            }
            return cust;
        }

        ///**************************************************************************************************
        ///<summary> Gets IncCmplete Customer by identifier.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<param name="id"> .</param>
        ///<returns> The by identifier.</returns>
        ///*************************************************************************************************
        public Customer GetByIdFullInComplete(int id)
        {
            cmd.CommandText = "dbo.Customer_GetByIdFullIncomplete";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@id", id));

            Customer cust = new Customer();

            try
            {
                SqlDataReader results = cmd.ExecuteReader();
                cmd.Parameters.Clear();

                while (results.Read())
                {
                    cust.Id = results.GetInt32(0);
                    cust.Firstname = results.GetString(1);
                    cust.Lastname = results.GetString(2);
                    cust.Phone = results.GetString(3);
                    cust.Email = results.GetString(4);
                    cust.Version = results.GetInt32(5);
                    cust.AddressId = results.GetInt32(6);
                    cust.Street = results.GetString(7);
                    cust.City = results.GetString(8);
                    cust.PostalCode = results.GetString(9);
                    cust.Country = results.GetString(10);
                }
                results.Close();
            }
            catch (SqlException ex)
            {
                Console.WriteLine("Error in CustomerDAO, GetByIdFull  " + ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error in CustomerDAO, GetByIdFull  " + ex.Message);
            }
            return cust;
        }

        ///**************************************************************************************************
        ///<summary> Deletes Customer given ID.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<param name="id"> .</param>
        ///*************************************************************************************************
        public void Delete(int id)
        {
            cmd.CommandText = "dbo.Customer_Delete";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@id", id));

            try
            {
                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();
            }
            catch (SqlException ex)
            {
                Console.WriteLine("Error in CustomerDAO, Delete  " + ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error in CustomerDAO, Delete  " + ex.Message);
            }
        }

        ///**************************************************************************************************
        ///<summary> Updates the given cust.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<param name="cust"> .</param>
        ///<returns>Customer.</returns>
        ///*************************************************************************************************
        public Customer Update(Customer cust)
        {
            cmd.CommandText = "dbo.Customer_Update";
            cmd.CommandType = CommandType.StoredProcedure;

            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@fname", cust.Firstname));
            parameters.Add(new SqlParameter("@lname", cust.Lastname));
            parameters.Add(new SqlParameter("@phone", cust.Phone));
            parameters.Add(new SqlParameter("@email", cust.Email));
            parameters.Add(new SqlParameter("@tablenum", cust.TableNum));
            parameters.Add(new SqlParameter("@id", cust.Id));
            cmd.Parameters.AddRange(parameters.ToArray());

            try
            {
                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();
            }
            catch (SqlException ex)
            {
                Console.WriteLine("Error in CustomerDAO, Update  " + ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error in CustomerDAO, Update  " + ex.Message);
            }
            cust.Version++;

            return cust;
        }

        ///**************************************************************************************************
        ///<summary> Updates full Customer.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<param name="cust"> .</param>
        ///<returns>Customer.</returns>
        ///*************************************************************************************************
        public Customer UpdateFull(Customer cust)
        {
            cmd.CommandText = "dbo.Customer_UpdateFull";
            cmd.CommandType = CommandType.StoredProcedure;

            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@fname", cust.Firstname));
            parameters.Add(new SqlParameter("@lname", cust.Lastname));
            parameters.Add(new SqlParameter("@phone", cust.Phone));
            parameters.Add(new SqlParameter("@email", cust.Email));
            parameters.Add(new SqlParameter("@street", cust.Street));
            parameters.Add(new SqlParameter("@city", cust.City));
            parameters.Add(new SqlParameter("@pc", cust.PostalCode));
            parameters.Add(new SqlParameter("@country", cust.Country));
            parameters.Add(new SqlParameter("@id", cust.Id));
            cmd.Parameters.AddRange(parameters.ToArray());

            try
            {
                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();
            }
            catch (SqlException ex)
            {
                Console.WriteLine("Error in CustomerDAO, UpdateFull  " + ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error in CustomerDAO, UpdateFull  " + ex.Message);
            }
            cust.Version++;
            return cust;
        }

        ///**************************************************************************************************
        ///<summary> Gets all Customers.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<returns> all.</returns>
        ///*************************************************************************************************
        public List<Customer> GetAll()
        {
            cmd.CommandText = "dbo.Customer_GetAll";
            cmd.CommandType = CommandType.StoredProcedure;
            List<Customer> list = new List<Customer>();

            try
            {
                SqlDataReader results = cmd.ExecuteReader();
                while (results.Read())
                {
                    Customer cust = new Customer();
                    cust.Id = results.GetInt32(0);
                    cust.Firstname = results.GetString(1);
                    cust.Lastname = results.GetString(2);
                    cust.Phone = results.GetString(3);
                    cust.Email = results.GetString(4);
                    cust.Version = results.GetInt32(5);
                    cust.TableNum = results.GetInt32(6);
                    list.Add(cust);
                }
                results.Close();
            }
            catch (SqlException ex)
            {
                Console.WriteLine("Error in CustomerDAO, GetAll  " + ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error in CustomerDAO, GetAll  " + ex.Message);
            }

            return list;
        }

        ///**************************************************************************************************
        ///<summary> Gets all full Customers.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<returns> all full.</returns>
        ///*************************************************************************************************
        public List<Customer> GetAllFull()
        {
            cmd.CommandText = "dbo.Customer_GetAllFull";
            cmd.CommandType = CommandType.StoredProcedure;
            List<Customer> list = new List<Customer>();

            try
            {
                SqlDataReader results = cmd.ExecuteReader();
                while (results.Read())
                {
                    Customer cust = new Customer();
                    cust.Id = results.GetInt32(0);
                    cust.Firstname = results.GetString(1);
                    cust.Lastname = results.GetString(2);
                    cust.Phone = results.GetString(3);
                    cust.Email = results.GetString(4);
                    cust.Version = results.GetInt32(5);
                    cust.AddressId = results.GetInt32(6);
                    cust.Street = results.GetString(7);
                    cust.City = results.GetString(8);
                    cust.PostalCode = results.GetString(9);
                    cust.Country = results.GetString(10);
                    list.Add(cust);
                }
                results.Close();
            }
            catch (SqlException ex)
            {
                Console.WriteLine("Error in CustomerDAO, GetAllFull  " + ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error in CustomerDAO, GetAllFull  " + ex.Message);
            }

            return list;
        }

        ///**************************************************************************************************
        ///<summary> Gets all full inComplete Customers.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<returns> all full inComplete.</returns>
        ///*************************************************************************************************
        public List<Customer> GetAllFullInComplete()
        {
            cmd.CommandText = "dbo.Customer_GetAllFullIncomplete";
            cmd.CommandType = CommandType.StoredProcedure;
            List<Customer> list = new List<Customer>();

            try
            {
                SqlDataReader results = cmd.ExecuteReader();
                while (results.Read())
                {
                    Customer cust = new Customer();
                    cust.Id = results.GetInt32(0);
                    cust.Firstname = results.GetString(1);
                    cust.Lastname = results.GetString(2);
                    cust.Phone = results.GetString(3);
                    cust.Email = results.GetString(4);
                    cust.Version = results.GetInt32(5);
                    cust.AddressId = results.GetInt32(6);
                    cust.Street = results.GetString(7);
                    cust.City = results.GetString(8);
                    cust.PostalCode = results.GetString(9);
                    cust.Country = results.GetString(10);
                    list.Add(cust);
                }
                results.Close();
            }
            catch (SqlException ex)
            {
                Console.WriteLine("Error in CustomerDAO, GetAllFull  " + ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error in CustomerDAO, GetAllFull  " + ex.Message);
            }

            return list;
        }

        ///**************************************************************************************************
        ///<summary> Adds an item to Customer.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<param name="custId"> [in] Identifier for the customer.</param>
        ///<param name="itemId"> [in] Identifier for the item.</param>
        ///*************************************************************************************************
        public void add_Item(int custId,int itemId)
        {
            cmd.CommandText = "dbo.Customer_Add_Item";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@custid", custId));
            cmd.Parameters.Add(new SqlParameter("@itemid", itemId));

            try
            {
               cmd.ExecuteNonQuery();
               cmd.Parameters.Clear();
            }
            catch (SqlException ex)
            {
                Console.WriteLine("Error in CustomerDAO, AddItem  " + ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error in CustomerDAO, AddItem  " + ex.Message);
            }
        }

        ///**************************************************************************************************
        ///<summary> Adds an item to a Customer who has not paid.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<param name="custId"> [in] Identifier for the customer.</param>
        ///<param name="itemId"> [in] Identifier for the item.</param>
        ///*************************************************************************************************
        public void add_ItemInComplete(int custId, int itemId)
        {
            cmd.CommandText = "dbo.Customer_Add_ItemIncomplete";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@custid", custId));
            cmd.Parameters.Add(new SqlParameter("@itemid", itemId));

            try
            {
                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();
            }
            catch (SqlException ex)
            {
                Console.WriteLine("Error in CustomerDAO, AddItemInComplete  " + ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error in CustomerDAO, AddItemInComplete  " + ex.Message);
            }
        }

        ///**************************************************************************************************
        ///<summary> Pays Customers bill.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<param name="custId"> [in] Identifier for the customer.</param>
        ///*************************************************************************************************
        public void pay_Bill(int custId)
        {
            cmd.CommandText = "dbo.Customer_PayBill";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@id", custId));

            try
            {
                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();
            }
            catch (SqlException ex)
            {
                Console.WriteLine("Error in CustomerDAO, pay_bill  " + ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error in CustomerDAO, pay_bill " + ex.Message);
            }
        }

        ///**************************************************************************************************
        ///<summary> Gets Customers bill.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<param name="id"> .</param>
        ///<returns> The bill.</returns>
        ///*************************************************************************************************
        public List<Item> GetBill(int id)
        {
            List<Item> result = new List<Item>();

            cmd.CommandText = "dbo.Customer_GetBill";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@id",id));

            try
            {
                SqlDataReader results = cmd.ExecuteReader();
                while (results.Read())
                {
                    Item itm = new Item();
                    itm.Id = results.GetInt32(0);
                    itm.Name = results.GetString(1);
                    itm.Price = results.GetSqlMoney(2).ToDouble();
                    result.Add(itm);
                }
                results.Close();
            }
            catch (SqlException ex)
            {
                Console.WriteLine("Error in CustomerDAO, GetBill  " + ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error in CustomerDAO, GetBill  " + ex.Message);
            }
            return result;
        }
    }
}