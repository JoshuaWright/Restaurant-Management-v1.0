﻿///**************************************************************************************************
/// file:	Employee.cs
/// <author>Joshua Wright</author>
/// <date>2016-11-06</date>
///*************************************************************************************************
namespace RestaurantDAL
{
    ///**************************************************************************************************
    ///<summary> An employee Entity.</summary>
    ///<remarks> Joshua Wright, 2016-11-06.</remarks>
    ///*************************************************************************************************
    public class Employee
    {
        public int Id { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public int Version { get; set; }
        public double PayRate { get; set; }
        public int HoursWorked { get; set; }
        public int AddressId { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
    }
}
