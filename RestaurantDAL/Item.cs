﻿///**************************************************************************************************
/// file:	Item.cs
/// <author>Joshua Wright</author>
/// <date>2016-11-06</date>
///*************************************************************************************************
namespace RestaurantDAL
{
    ///**************************************************************************************************
    ///<summary> An item Entity.</summary>
    ///<remarks> Joshua Wright, 2016-11-06.</remarks>
    ///*************************************************************************************************
    public class Item
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public int Version { get; set; }
    }
}
