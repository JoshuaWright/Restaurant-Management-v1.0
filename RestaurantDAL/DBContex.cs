﻿///**************************************************************************************************
/// file:	DBContex.cs
/// <author>Joshua Wright</author>
/// <date>2016-11-06</date>
///*************************************************************************************************
using System.Data.SqlClient;

namespace RestaurantDAL
{
    ///**************************************************************************************************
    ///<summary> A database contex.</summary>
    ///<remarks> Joshua Wright, 2016-11-06.</remarks>
    ///*************************************************************************************************
    public class DBContex
    {
        private SqlConnection con_;
        public System.Data.SqlClient.SqlConnection Con
        {
            get { return con_; }
        }
        public DBContex()
        {
            con_ = new SqlConnection("Data Source=DESKTOP-R1N4DHM;Initial Catalog=Restaurantv1.0;Integrated Security=True");
            con_.Open();
        }
    }
}
