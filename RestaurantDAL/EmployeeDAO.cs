﻿///**************************************************************************************************
/// file:	EmployeeDAO.cs
/// <author>Joshua Wright</author>
/// <date>2016-11-06</date>
///*************************************************************************************************
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System;

namespace RestaurantDAL
{
    ///**************************************************************************************************
    ///<summary> An employee dao.</summary>
    ///<remarks> Joshua Wright, 2016-11-06.</remarks>
    ///*************************************************************************************************
    public class EmployeeDAO
    {
        private DBContex dao_;
        private SqlCommand cmd;


        public EmployeeDAO()
        {
            dao_ = new DBContex();
            cmd = new SqlCommand();
            cmd.Connection = dao_.Con;
        }

        ///**************************************************************************************************
        ///<summary> Creates a new Employee.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<param name="emp"> The emp.</param>
        ///<returns> An Employee.</returns>
        ///*************************************************************************************************
        public Employee Create(Employee emp)
        {
            cmd.CommandText = "dbo.Employee_FullInsert";
            cmd.CommandType = CommandType.StoredProcedure;

            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@fname", emp.Firstname));
            parameters.Add(new SqlParameter("@lname", emp.Lastname));
            parameters.Add(new SqlParameter("@phone", emp.Phone));
            parameters.Add(new SqlParameter("@email", emp.Email));
            parameters.Add(new SqlParameter("@pr", emp.PayRate));
            parameters.Add(new SqlParameter("@street", emp.Street));
            parameters.Add(new SqlParameter("@city", emp.City));
            parameters.Add(new SqlParameter("@pc", emp.PostalCode));
            parameters.Add(new SqlParameter("@country", emp.Country));

            cmd.Parameters.AddRange(parameters.ToArray());

            try
            {
                emp.Id = cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();
            }
            catch (SqlException ex)
            {
                Console.WriteLine("Error in EmployeeDAO, Create  " + ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error in EmployeeDAO, Create  " + ex.Message);
            }
            emp.Version = 1;

            return emp;
        }

        ///**************************************************************************************************
        ///<summary> Gets Employee by identifier.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<param name="id"> The identifier.</param>
        ///<returns> The by identifier.</returns>
        ///*************************************************************************************************
        public Employee GetById(int id)
        {
            cmd.CommandText = "dbo.Employee_GetById";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@id", id));

            Employee emp = new Employee();

            try
            {
                SqlDataReader results = cmd.ExecuteReader();
                cmd.Parameters.Clear();

                while (results.Read())
                {
                    emp.Id = results.GetInt32(0);
                    emp.Firstname = results.GetString(1);
                    emp.Lastname = results.GetString(2);
                    emp.Phone = results.GetString(3);
                    emp.Email = results.GetString(4);
                    emp.Version = results.GetInt32(5);
                    emp.PayRate = results.GetSqlMoney(6).ToDouble();
                    emp.HoursWorked = results.GetInt32(7);
                    emp.AddressId = results.GetInt32(8);
                    emp.Street = results.GetString(9);
                    emp.City = results.GetString(10);
                    emp.PostalCode = results.GetString(11);
                    emp.Country = results.GetString(12);
                }
                results.Close();
            }
            catch (SqlException ex)
            {
                Console.WriteLine("Error in EmployeeDAO, GetById  " + ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error in EmployeeDAO, GetById  " + ex.Message);
            }
            return emp;
        }

        ///**************************************************************************************************
        ///<summary> Deletes Employee given ID.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<param name="id"> The identifier.</param>
        ///*************************************************************************************************
        public void Delete(int id)
        {
            cmd.CommandText = "dbo.Employee_FullDelete";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@id", id));
            try
            {
                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();
            }
            catch (SqlException ex)
            {
                Console.WriteLine("Error in EmployeeDAO, Delete  " + ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error in EmployeeDAO, Delete  " + ex.Message);
            }
        }

        ///**************************************************************************************************
        ///<summary> Gets all Employees.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<returns> all.</returns>
        ///*************************************************************************************************
        public List<Employee> GetAll()
        {
            cmd.CommandText = "dbo.Employee_GetAll";
            cmd.CommandType = CommandType.StoredProcedure;
            List<Employee> list = new List<Employee>();

            try
            {
                SqlDataReader results = cmd.ExecuteReader();
                while (results.Read())
                {
                    Employee emp = new Employee();
                    emp.Id = results.GetInt32(0);
                    emp.Firstname = results.GetString(1);
                    emp.Lastname = results.GetString(2);
                    emp.Phone = results.GetString(3);
                    emp.Email = results.GetString(4);
                    emp.Version = results.GetInt32(5);
                    emp.PayRate = results.GetSqlMoney(6).ToDouble();
                    emp.HoursWorked = results.GetInt32(7);
                    emp.AddressId = results.GetInt32(8);
                    emp.Street = results.GetString(9);
                    emp.City = results.GetString(10);
                    emp.PostalCode = results.GetString(11);
                    emp.Country = results.GetString(12);
                    list.Add(emp);
                }
                results.Close();
            }
            catch (SqlException ex)
            {
                Console.WriteLine("Error in EmployeeDAO, GetAll  " + ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error in EmployeeDAO, GetAll  " + ex.Message);
            }

            return list;
        }

        ///**************************************************************************************************
        ///<summary> Updates the given emp.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<param name="emp"> The emp.</param>
        ///<returns> An Employee.</returns>
        ///*************************************************************************************************
        public Employee Update(Employee emp)
        {
            cmd.CommandText = "dbo.Employee_Update";
            cmd.CommandType = CommandType.StoredProcedure;

            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@fname", emp.Firstname));
            parameters.Add(new SqlParameter("@lname", emp.Lastname));
            parameters.Add(new SqlParameter("@phone", emp.Phone));
            parameters.Add(new SqlParameter("@email", emp.Email));
            parameters.Add(new SqlParameter("@pr", emp.PayRate));
            parameters.Add(new SqlParameter("@hr", emp.HoursWorked));
            parameters.Add(new SqlParameter("@street", emp.Street));
            parameters.Add(new SqlParameter("@city", emp.City));
            parameters.Add(new SqlParameter("@pc", emp.PostalCode));
            parameters.Add(new SqlParameter("@country", emp.Country));
            parameters.Add(new SqlParameter("@id", emp.Id));
            cmd.Parameters.AddRange(parameters.ToArray());

            try
            {
                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();
            }
            catch (SqlException ex)
            {
                Console.WriteLine("Error in EmployeeDAO, Update  " + ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error in EmployeeDAO, Update  " + ex.Message);
            }
            emp.Version++;
            return emp;
        }
    }
}
