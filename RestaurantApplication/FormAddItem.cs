﻿///**************************************************************************************************
/// file:	FormAddItem.cs
/// <author>Joshua Wright</author>
/// <date>2016-11-06</date>
///*************************************************************************************************
using RestaurantVML;
using System;
using System.Windows.Forms;

namespace RestaurantApplication
{
    public partial class FormAddItem : Form
    {
        public FormAddItem(string AU)
        {
            InitializeComponent();
            this.ActiveControl = button1;

            //Checks what string was passed through Constructor
            ivm = new ItemVM();
            if (AU == "A")
            {
                this.button1.Text = "Add new Item";
            }
            else
            {
                this.button1.Text = "Update Item";
                this.Text = "Update Item";
                ivm.Id = int.Parse(AU);
                ivm.GetById();
                this.textBox1.Text = ivm.Name;
                this.textBox2.Text = ""+ivm.Price;
                this.button1.ForeColor = System.Drawing.Color.Blue;
            }
        } 

        ///**************************************************************************************************
        ///<summary> Event handler. Called by button1(Add or Update Button) for click events.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<param name="sender"> [in] Source of the event.</param>
        ///<param name="e">      [in] Event information.</param>
        ///*************************************************************************************************
        private void button1_Click(object sender, EventArgs e)
        {
            //checks that price text box has numeric value
            double temp = 0;
            if(!double.TryParse(this.textBox2.Text,out temp))
            {
                MessageBox.Show("Price must be Numeric", "Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
           
            UpdateAdd fp = new UpdateAdd(ivm.Update);

            if (this.button1.Text == "Add new Item")
                fp = ivm.Create;
               
            ivm.Name = this.textBox1.Text;
            ivm.Price = double.Parse(this.textBox2.Text);
            fp();
            this.Hide();
            this.Close();
        }
    }
}
