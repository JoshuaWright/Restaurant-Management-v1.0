﻿///**************************************************************************************************
/// file:	FormViewBill.cs
/// <author>Joshua Wright</author>
/// <date>2016-11-06</date>
///*************************************************************************************************
using RestaurantVML;
using System;
using System.Windows.Forms;

namespace RestaurantApplication
{
    public partial class FormViewBill : Form
    {
        ///**************************************************************************************************
        ///<summary>Initializes a new instance of the RestaurantApplication.FormViewBill class.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<param name="id"> [in] The identifier.</param>
        ///*************************************************************************************************
        public FormViewBill(int id)
        {
            InitializeComponent();
            InitializeComponentID(id);
        }

        ///**************************************************************************************************
        ///<summary> Initializes a new instance of the RestaurantApplication.FormViewBill class.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<param name="cust"> [in] The customer.</param>
        ///*************************************************************************************************
        public FormViewBill(CustomerVM cust)
        {
            InitializeComponent();
            InitializeComponentVM(cust);
        }

        ///**************************************************************************************************
        ///<summary> Event handler. Called by button1(Ok) for click events.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<param name="sender"> [in] Source of the event.</param>
        ///<param name="e">      [in] Event information.</param>
        ///*************************************************************************************************
        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            this.Dispose();
        }

        ///**************************************************************************************************
        ///<summary> Event handler. Called by button7(Receipt) for click events.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<param name="sender"> [in] Source of the event.</param>
        ///<param name="e">      [in] Event information.</param>
        ///*************************************************************************************************
        private void button7_Click(object sender, EventArgs e)
        {
            FormReceipt f = new FormReceipt(vm);
            f.ShowDialog();
        }
    }
}
