﻿using RestaurantVML;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RestaurantApplication
{
    public partial class FormReceipt : Form
    {
        ///**************************************************************************************************
        ///<summary>Initializes a new instance of the RestaurantApplication.FormReceipt class.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<param name="cust"> [in] The customer.</param>
        ///*************************************************************************************************
        public FormReceipt(CustomerVM cust)
        {
            cvm = cust;
            InitializeComponent();
            InitializeComponent2();
        }

        ///**************************************************************************************************
        ///<summary> Event handler. Called by textBox1 for text changed events.
        ///          (to make form size dynamic)
        ///</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<param name="sender"> [in] Source of the event.</param>
        ///<param name="e">      [in] Event information.</param>
        ///*************************************************************************************************
        void textBox1_TextChanged(object sender, EventArgs e)
        {
            Size sz = new Size(textBox1.ClientSize.Width, int.MaxValue);
            TextFormatFlags flags = TextFormatFlags.WordBreak;
            int padding = 3;
            int borders = textBox1.Height - textBox1.ClientSize.Height;
            sz = TextRenderer.MeasureText(textBox1.Text, textBox1.Font, sz, flags);
            int h = sz.Height + borders + padding;
            if (textBox1.Top + h > this.ClientSize.Height - 10)
            {
                h = this.ClientSize.Height - 10 - textBox1.Top;
            }
            textBox1.Height = h;
            this.ClientSize = new Size(this.ClientSize.Width, textBox1.Height);
        }
    }
}
