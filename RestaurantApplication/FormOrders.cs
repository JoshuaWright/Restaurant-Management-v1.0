﻿///**************************************************************************************************
/// file:	FormOrders.cs
/// <author>Joshua Wright</author>
/// <date>2016-11-06</date>
///*************************************************************************************************
using RestaurantVML;
using System;
using System.Windows.Forms;

namespace RestaurantApplication
{
    public partial class FormOrders : Form
    {
        public FormOrders()
        {
            InitializeComponent();
            InitializeComponent2();
        }

        ///**************************************************************************************************
        ///<summary> Event handler. Called by button1(Creates an order) for click events.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<param name="sender"> [in] Source of the event.</param>
        ///<param name="e">      [in] Event information.</param>
        ///*************************************************************************************************
        private void button1_Click(object sender, EventArgs e)
        {
            // checks all fields for data
            for (int i = 0; i < 8; ++i)
                if (string.IsNullOrEmpty(textBox[i].Text))
                {
                    MessageBox.Show("All fields must be filled", "error",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
         
            cust.Firstname = textBox[0].Text;
            cust.Lastname = textBox[1].Text;
            cust.Phone = textBox[2].Text;
            cust.Email = textBox[3].Text;
            cust.Street = textBox[4].Text;
            cust.City = textBox[5].Text;
            cust.PostalCode = textBox[6].Text;
            cust.Country = textBox[7].Text;
            cust.CreateFullInComplete();
            this.Hide();
            this.Dispose();
        }

        ///**************************************************************************************************
        ///<summary> Refresh list view 1(Menu).</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///*************************************************************************************************
        public void refreshlistview1()
        {
            listView1.Items.Clear();
            var ilist = ivm.GetAll();
            foreach (ItemVM vm in ilist)
            {
                ListViewItem i = new ListViewItem("" + vm.Id);
                i.SubItems.Add(vm.Name);
                i.SubItems.Add("$" + vm.Price);
                this.listView1.Items.Add(i);
            }
        }

        ///**************************************************************************************************
        ///<summary> Refresh list view 2(Bill).</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///*************************************************************************************************
        public void refreshlistview2()
        {
            listView2.Items.Clear();
            foreach (ItemVM vm in cust.Bill)
            {
                ListViewItem i = new ListViewItem("" + vm.Id);
                i.SubItems.Add(vm.Name);
                i.SubItems.Add("$" + vm.Price);
                this.listView2.Items.Add(i);
            }
            textBox11.Text = "$"+cust.get_Bill_total();
        }

        ///**************************************************************************************************
        ///<summary> Event handler. Called by button2(add Item(S)) for click events.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<param name="sender"> [in] Source of the event.</param>
        ///<param name="e">      [in] Event information.</param>
        ///*************************************************************************************************
        private void button2_Click(object sender, EventArgs e)
        {
            if (this.listView1.SelectedItems.Count == 0)
            {
                MessageBox.Show("An Item Must Be Selected to be Added", "Error",
                     MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {            
                for (int index = 0; index < listView1.SelectedItems.Count; ++index)
                {
                    int id = int.Parse(listView1.SelectedItems[index].SubItems[0].Text);
                    ItemVM i = new ItemVM();
                    i.Id = id;
                    i.GetById();
                    cust.Bill.Add(i);
                }              
                refreshlistview2();
            }
        }

        ///**************************************************************************************************
        ///<summary> Event handler. Called by button3(remove item(S)) for click events.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<param name="sender"> [in] Source of the event.</param>
        ///<param name="e">      [in] Event information.</param>
        ///*************************************************************************************************
        private void button3_Click(object sender, EventArgs e)
        {
            if (this.listView2.SelectedItems.Count == 0)
            {
                MessageBox.Show("An Item Must Be Selected to be removed", "Error",
                     MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            else
            {
                int[] TBD = new int[listView2.SelectedItems.Count];
                for(int i =0; i < TBD.Length; ++i)
                {
                    TBD[i] = int.Parse(listView2.SelectedItems[i].SubItems[0].Text);
                }                      
                for (int i = 0; i < TBD.Length; ++i)
                {
                    cust.Bill.RemoveAt(cust.Bill.FindIndex(x => x.Id == TBD[i]));                                                     
                }              
                refreshlistview2();
            }
        }
    }
}
