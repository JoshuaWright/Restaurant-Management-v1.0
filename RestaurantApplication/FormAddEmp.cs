﻿///**************************************************************************************************
/// file:	FormAddEmp.cs
/// <author>Joshua Wright</author>
/// <date>2016-11-06</date>
///*************************************************************************************************
using System;
using System.Windows.Forms;
using RestaurantVML;

namespace RestaurantApplication
{
    public partial class FormAddEmp : Form
    {
        public FormAddEmp()
        {
            InitializeComponent();
            InitializeComponent2();
        }

        ///**************************************************************************************************
        ///<summary> Event handler. Called by button1(OK button) for click events.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<param name="sender"> [in] Source of the event.</param>
        ///<param name="e">      [in] Event information.</param>
        ///*************************************************************************************************
        private void button1_Click(object sender, EventArgs e)
        {
            // Checks all text boxes for data
            for (int i = 0; i < 9; ++i)
                if (string.IsNullOrEmpty(textBox[i].Text))
                {
                    MessageBox.Show("All fields must be filled", "error",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

            //checks Pay rate text box for Numeric value
            double temp = 0;
            if (!double.TryParse(this.textBox[4].Text, out temp))
            {
                MessageBox.Show("PayRate must be Numeric", "Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }


            EmployeeVM emp = new EmployeeVM();
            emp.Firstname = textBox[0].Text;
            emp.Lastname = textBox[1].Text;
            emp.Phone = textBox[2].Text;
            emp.Email = textBox[3].Text;
            emp.PayRate = double.Parse(textBox[4].Text);
            emp.Street = textBox[5].Text;
            emp.City = textBox[6].Text;
            emp.PostalCode = textBox[7].Text;
            emp.Country = textBox[8].Text;
            emp.Create();
            this.Hide();
            this.Dispose();                     
        }
    }
}
