﻿///**************************************************************************************************
/// file:	FormMain.cs
/// <author>Joshua Wright</author>
/// <date>2016-11-06</date>
///*************************************************************************************************
using System;
using System.Drawing;
using System.Windows.Forms;
using RestaurantVML;

namespace RestaurantApplication
{
    public partial class FormMain : Form
    {
        public FormMain()
        {
            InitializeComponent();
            InitializeComponent2();
        }

        ///**************************************************************************************************
        ///<summary> Refresh listview 1(Items tab).</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///*************************************************************************************************
        public void refreshlistview1()
        {
            listView1.Items.Clear();
            var ilist = ivm.GetAll();
            foreach (ItemVM vm in ilist)
            {
                ListViewItem i = new ListViewItem("" + vm.Id);
                i.SubItems.Add(vm.Name);
                i.SubItems.Add("$" + vm.Price);
                this.listView1.Items.Add(i);
            }
        }

        ///**************************************************************************************************
        ///<summary> Refresh listview 2 (Employee tab).</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///*************************************************************************************************
        public void refreshlistview2()
        {
            this.listView2.Items.Clear();
            var ilist = evm.GetAll();
            foreach (EmployeeVM vm in ilist)
            {
                ListViewItem i = new ListViewItem("" + vm.Id);
                i.SubItems.Add(vm.Lastname);
                this.listView2.Items.Add(i);
            }
        }

        ///**************************************************************************************************
        ///<summary> Refresh listview 3(Orders tab).</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///*************************************************************************************************
        public void refreshlistview3()
        {
            this.listView3.Items.Clear();
            var ilist = cvm.GetAllFullInComplete();
            if (ilist.Count != 0)
            {
                foreach (CustomerVM vm in ilist)
                {
                    ListViewItem i = new ListViewItem(""+vm.Id);
                    i.SubItems.Add(vm.Lastname);
                    i.SubItems.Add(vm.Phone);
                    i.SubItems.Add(vm.Email);
                    i.SubItems.Add(vm.Street);
                    i.SubItems.Add(vm.PostalCode);
                    this.listView3.Items.Add(i);
                }
                this.label16.Text = "Orders Loaded";
            }
            else
                this.label16.Text = "No Orders";
        }

        ///**************************************************************************************************
        ///<summary> Event handler. Called by button5(Add Item) for click events.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<param name="sender"> [in] Source of the event.</param>
        ///<param name="e">      [in] Event information.</param>
        ///*************************************************************************************************
        private void button5_Click(object sender, EventArgs e)
        {
            FormAddItem fi = new FormAddItem("A");
            label13.Text = "";
            fi.ShowDialog();
            listViewCount = this.listView1.Items.Count;
            if (!fi.Visible)
            {
                refreshlistview1();
                if (this.listView1.Items.Count > listViewCount)
                    label13.Text = "Item Added";
            }
        }

        ///**************************************************************************************************
        ///<summary> Event handler. Called by button1_Click(add Employee) for 1 events.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<param name="sender"> [in] Source of the event.</param>
        ///<param name="e">      [in] Event information.</param>
        ///*************************************************************************************************
        private void button1_Click_1(object sender, EventArgs e)
        {
            label1.Text = "";
            FormAddEmp f2 = new FormAddEmp();
            f2.ShowDialog();
            listViewCount = this.listView2.Items.Count;
            if (!f2.Visible)
            {
                refreshlistview2();
                if (this.listView2.Items.Count > listViewCount)
                    label1.Text = "Employee Added";
            }   
        }

        ///**************************************************************************************************
        ///<summary> Event handler. Called by button4(add Order) for click events.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<param name="sender"> [in] Source of the event.</param>
        ///<param name="e">      [in] Event information.</param>
        ///*************************************************************************************************
        private void button4_Click(object sender, EventArgs e)
        {
            label16.Text = "";
            FormOrders fo = new FormOrders();
            fo.ShowDialog();
            listViewCount = this.listView3.Items.Count;
            if (!fo.Visible)
            {
                refreshlistview3();
                if (this.listView3.Items.Count > listViewCount)
                    label16.Text = "Order Added";
            }
        }

        ///**************************************************************************************************
        ///<summary> Event handler. Called by Table_button(for Every table in Table tab) for click events.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<param name="sender"> [in] Source of the event.</param>
        ///<param name="e">      [in] Event information.</param>
        ///*************************************************************************************************
        private void Table_button_Click(object sender, EventArgs e)
        {
            TableInUse = CustomerVM.TableCluster.Count;
            FormTable f = new FormTable( int.Parse(((Button)sender).Text.Substring(6)));
            f.ShowDialog();
            if (f.Visible == false && CustomerVM.TableCluster.Count > TableInUse)
                ((Button)sender).BackColor = Color.LimeGreen;
            else if (f.Visible == false && CustomerVM.TableCluster.Count < TableInUse)
                ((Button)sender).BackColor = Color.Transparent;
        }

        ///**************************************************************************************************
        ///<summary> Event handler. Called by button17(Update Item) for click events.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<param name="sender"> [in] Source of the event.</param>
        ///<param name="e">      [in] Event information.</param>
        ///*************************************************************************************************
        private void button17_Click(object sender, EventArgs e)
        {
            if (this.listView1.SelectedItems.Count == 0)
            {
                MessageBox.Show("An Item Must Be Selected to be Updated", "Error",
                     MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                FormAddItem fi = new FormAddItem("" + listView1.SelectedItems[0].SubItems[0].Text);

                ivm.Id = int.Parse(listView1.SelectedItems[0].SubItems[0].Text);
                ivm.GetById();
                tempVersion = ivm.Version;
                label13.Text = "";

                fi.ShowDialog();
               
                if (!fi.Visible)
                {
                    ivm.GetById();
                    refreshlistview1();
                    if (ivm.Version > tempVersion)
                        label13.Text = "Item Updated";
                }            
            }
        }

        ///**************************************************************************************************
        ///<summary> Event handler. Called by button18(delete item) for click events.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<param name="sender"> [in] Source of the event.</param>
        ///<param name="e">      [in] Event information.</param>
        ///*************************************************************************************************
        private void button18_Click(object sender, EventArgs e)
        {
            if (this.listView1.SelectedItems.Count == 0)
            {
               MessageBox.Show("An Item Must Be Selected to be Deleted","Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);                           
            }
            else
            {
                if (MessageBox.Show("Are you sure you want to delete this item", "Warning",
                   MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No)
                    return;

                ItemVM ivm = new ItemVM();
                ivm.Id = int.Parse(this.listView1.SelectedItems[0].SubItems[0].Text);
                ivm.delete();
                listViewCount = this.listView1.Items.Count;

                refreshlistview1();
                if (this.listView1.Items.Count < listViewCount)
                    label13.Text = "Item Deleted";         
            }
        }

        ///**************************************************************************************************
        ///<summary> Event handler. Called by button19(view Employee) for click events.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<param name="sender"> [in] Source of the event.</param>
        ///<param name="e">      [in] Event information.</param>
        ///*************************************************************************************************
        private void button19_Click(object sender, EventArgs e)
        {
            label1.Text = "";
            if (listView2.SelectedItems.Count > 0)
            {
                evm.Id = int.Parse(listView2.SelectedItems[0].SubItems[0].Text);
                evm.GetById();
                this.textBox2.Text = evm.Lastname;
                this.textBox1.Text = evm.Firstname;
                this.textBox3.Text = evm.Phone;
                this.textBox4.Text = evm.Email;
                this.textBox5.Text = "" + evm.PayRate;
                this.textBox7.Text = "" + evm.HoursWorked;
                this.textBox11.Text = evm.Street;
                this.textBox10.Text = evm.City;
                this.textBox8.Text = evm.PostalCode;
                this.textBox9.Text = evm.Country;

                label1.Text = "Employee Found";
            }
            else
            {
                MessageBox.Show("An Employee Must Be Selected to be Viewed", "Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        ///**************************************************************************************************
        ///<summary> Event handler. Called by button2(update Employee) for click events.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<param name="sender"> [in] Source of the event.</param>
        ///<param name="e">      [in] Event information.</param>
        ///*************************************************************************************************
        private void button2_Click(object sender, EventArgs e)
        {
            label1.Text = "";
            if (listView2.SelectedItems.Count == 0)
            {
                MessageBox.Show("An Employee Must be Selected to be updated", "error",
               MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            else if (string.IsNullOrEmpty(textBox2.Text) ||
                string.IsNullOrEmpty(textBox3.Text) ||
                 string.IsNullOrEmpty(textBox1.Text) ||
                string.IsNullOrEmpty(textBox4.Text) ||
                string.IsNullOrEmpty(textBox7.Text) ||
                string.IsNullOrEmpty(textBox8.Text) ||
                string.IsNullOrEmpty(textBox9.Text) ||
                string.IsNullOrEmpty(textBox10.Text) ||
                string.IsNullOrEmpty(textBox11.Text)
                )
            {
                MessageBox.Show("All fields must be filled To Update An Employee", "error",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            evm.Id = int.Parse(listView2.SelectedItems[0].SubItems[0].Text);
            evm.Lastname = textBox2.Text;
            evm.Firstname = textBox1.Text;
            evm.Phone = textBox3.Text;
            evm.Email = textBox4.Text;
            evm.PayRate = double.Parse(textBox5.Text);

            if (string.IsNullOrEmpty(textBox6.Text))
                evm.HoursWorked = int.Parse(textBox7.Text);
            else
                evm.HoursWorked = int.Parse(textBox7.Text) + int.Parse(textBox6.Text);

            evm.Street = textBox11.Text;
            evm.City = textBox10.Text;
            evm.PostalCode = textBox8.Text;
            evm.Country = textBox9.Text;
            evm.Update();

            label1.Text = "Employee Updated";
            refreshlistview2();
        }

        ///**************************************************************************************************
        ///<summary> Event handler. Called by button3(Delete Employee) for click events.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<param name="sender"> [in] Source of the event.</param>
        ///<param name="e">      [in] Event information.</param>
        ///*************************************************************************************************
        private void button3_Click(object sender, EventArgs e)
        {
            label1.Text = "";
            if (listView2.SelectedItems.Count == 0)
            {
                MessageBox.Show("An Employee Must be Selected to be Deleted", "error",
               MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (MessageBox.Show("Are you sure you want to delete this Employee", "Warning",
                  MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No)
                return;

            evm.Id = evm.Id = int.Parse(listView2.SelectedItems[0].SubItems[0].Text);
            evm.Delete();
            refreshlistview2();
            label1.Text = "Employee Deleted";
        }

        ///**************************************************************************************************
        ///<summary> Event handler. Called by button15(cancel Order) for click events.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<param name="sender"> [in] Source of the event.</param>
        ///<param name="e">      [in] Event information.</param>
        ///*************************************************************************************************
        private void button15_Click(object sender, EventArgs e)
        {
            label16.Text = "";
            if (listView3.SelectedItems.Count == 0)
            {
                MessageBox.Show("An Order Must be Selected to be Canceled", "error",
               MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (MessageBox.Show("Are you sure you want to cancel this Order", "Warning",
                  MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No)
                return;

            int index = 0;
            int id = int.Parse(listView3.SelectedItems[0].SubItems[0].Text);
            var list = cvm.GetAllFullInComplete();
            foreach (CustomerVM vm in list)
            {
                if (id == vm.Id)
                {
                    list[index].Delete();
                    break;
                }
                ++index;
            }

            refreshlistview3();
            label16.Text = "Order Canceled";
        }

        ///**************************************************************************************************
        ///<summary> Event handler. Called by button16(Order complete) for click events.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<param name="sender"> [in] Source of the event.</param>
        ///<param name="e">      [in] Event information.</param>
        ///*************************************************************************************************
        private void button16_Click(object sender, EventArgs e)
        {
            if (listView3.SelectedItems.Count == 0)
            {
                MessageBox.Show("An Order Must be Selected to be Completed", "error",
               MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            cvm.Id = int.Parse(listView3.SelectedItems[0].SubItems[0].Text);
            cvm.pay_bill();
            refreshlistview3();
            label16.Text = "Order Complete";
        }

        ///**************************************************************************************************
        ///<summary> Event handler. Called by button20(view order details) for click events.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<param name="sender"> [in] Source of the event.</param>
        ///<param name="e">      [in] Event information.</param>
        ///*************************************************************************************************
        private void button20_Click(object sender, EventArgs e)
        {
            if (listView3.SelectedItems.Count == 0)
            {
                MessageBox.Show("An Order Must be Selected to View Details", "error",
               MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            FormViewBill f = new FormViewBill(
                int.Parse(listView3.SelectedItems[0].SubItems[0].Text));
            f.ShowDialog();
        }   
    }
}
