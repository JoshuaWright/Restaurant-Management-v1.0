﻿///**************************************************************************************************
/// file:	FormManageTableBill.cs
/// <author>Joshua Wright</author>
/// <date>2016-11-06</date>
///*************************************************************************************************
using RestaurantVML;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace RestaurantApplication
{
    public partial class FormManageTableBill : Form
    {
        ///**************************************************************************************************
        ///<summary>Initializes a new instance of the RestaurantApplication.FormManageTableBill class.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<param name="tablenum"> [in] The table number.</param>
        ///*************************************************************************************************
        public FormManageTableBill(int tablenum)
        {
            TABLENUM = tablenum;
            InitializeComponent();
            InitializeComponent2();
            this.Text = "Table #" + TABLENUM + "'s Bill Management";
        }

        ///**************************************************************************************************
        ///<summary> Refresh list view 2(Table bill).</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///*************************************************************************************************
        private void refreshlistview2()
        {
            listView2.Items.Clear();
            cvm = CustomerVM.TableCluster[
                CustomerVM.TableCluster.FindIndex(x => x.TS.TableNum == TABLENUM)];

            foreach (ItemVM vm in cvm.Bill)
            {
                ListViewItem i = new ListViewItem("" + vm.Id);
                i.SubItems.Add(vm.Name);
                i.SubItems.Add("$" + vm.Price);
                this.listView2.Items.Add(i);
            }
            textBox1.Text = "$" + cvm.get_Bill_total();
        }

        ///**************************************************************************************************
        ///<summary> Refresh list view 1(customes).</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///*************************************************************************************************
        private void refreshlistview1()
        {
            listView1.Items.Clear();
            foreach (CustomerVM cst in 
                CustomerVM.TableCluster.FindAll(x => x.TS.TableNum == TABLENUM 
                                                        && x.TS.SeatNum!= -1))
            {
                ListViewItem i = new ListViewItem("" + cst.TS.SeatNum);
                this.listView1.Items.Add(i);
            }
        }

        ///**************************************************************************************************
        ///<summary> Event handler. Called by button4(add customer) for click events.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<param name="sender"> [in] Source of the event.</param>
        ///<param name="e">      [in] Event information.</param>
        ///*************************************************************************************************
        private void button4_Click(object sender, EventArgs e)
        {
            if(CustomerVM.TableSeatRecord.FindIndex(x=> x.TableNum == TABLENUM ) == -1)
            {
                CustomerVM.TableSeatRecord.Add(new CustomerVM.TSR(TABLENUM, 1));
                CustomerVM.TableCluster.Add(new CustomerVM()
                {
                    TS = new CustomerVM.TSR(TABLENUM, 1)
                });
            }
            else
            {
                var IEnum = CustomerVM.TableSeatRecord.Where(x => 
                                                x.TableNum == TABLENUM);

                int Max = IEnum.Max(x => x.SeatNum);

                CustomerVM.TableSeatRecord.Add(new CustomerVM.TSR(
                    TABLENUM,Max + 1));

                CustomerVM.TableCluster.Add(new CustomerVM()
                {
                    TS = new CustomerVM.TSR(TABLENUM,
                                Max + 1)
                });
            }
            refreshlistview1();
            lblStatus.Text = "Customer Added";
        }

        ///**************************************************************************************************
        ///<summary> Event handler. Called by button3(add item(S)) for click events.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<param name="sender"> [in] Source of the event.</param>
        ///<param name="e">      [in] Event information.</param>
        ///*************************************************************************************************
        private void button3_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count == 0 || listView2.SelectedItems.Count == 0)
            {
                MessageBox.Show("An Item Must Be Selected to be Added OR a Customer" +
                                        " Must Be selected", "Error",
                     MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            List<ItemVM> ilist = new List<ItemVM>();
            for (int index = 0; index < listView2.SelectedItems.Count; ++index)
            {
                int id = int.Parse(listView2.SelectedItems[index].SubItems[0].Text);
                ItemVM i = new ItemVM();
                i.Id = id;
                i.GetById();
                ilist.Add(i);
            }
            int seat = int.Parse(listView1.SelectedItems[0].SubItems[0].Text);

            CustomerVM.TableCluster.Find(x => x.TS.TableNum == TABLENUM
                                                && x.TS.SeatNum == seat)
                                                .Bill.AddRange(ilist);

            var TableBill = CustomerVM.TableCluster.Find(x => x.TS.TableNum == TABLENUM
                                            && x.TS.SeatNum == -1).Bill;

            foreach (ItemVM itm in ilist)
            {
                TableBill.RemoveAt(TableBill.FindIndex(x => x.Id == itm.Id));
            }
            refreshlistview2();
            lblStatus.Text = "Item(s) Added";
        }

        ///**************************************************************************************************
        ///<summary> Event handler. Called by button8(vew details) for click events.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<param name="sender"> [in] Source of the event.</param>
        ///<param name="e">      [in] Event information.</param>
        ///*************************************************************************************************
        private void button8_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count == 0)
            {
                MessageBox.Show("A Customer Must Be Selected", "Error",
                     MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            int seat = int.Parse(listView1.SelectedItems[0].SubItems[0].Text);

            FormViewBill f = new FormViewBill(CustomerVM.TableCluster.Find(x =>
                                                x.TS.TableNum == TABLENUM
                                                && x.TS.SeatNum == seat));

            f.ShowDialog();
        }

        ///**************************************************************************************************
        ///<summary> Event handler. Called by button5(remove customer) for click events.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<param name="sender"> [in] Source of the event.</param>
        ///<param name="e">      [in] Event information.</param>
        ///*************************************************************************************************
        private void button5_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count == 0)
            {
                MessageBox.Show("A Customer Must Be Removed", "Error",
                     MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (MessageBox.Show("Are you sure you want to remove this Customer", "Warning",
                  MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No)
                return;

            CustomerVM TBD = CustomerVM.TableCluster.Find(x => x.TS.TableNum == TABLENUM
                                 && x.TS.SeatNum ==
                                int.Parse(listView1.SelectedItems[0].SubItems[0].Text));
            CustomerVM.TableCluster.Find(x => x.TS.TableNum == TABLENUM
                                            && x.TS.SeatNum == -1)
            .Bill.AddRange(TBD.Bill);

            CustomerVM.TableCluster.Remove(TBD);

            refreshlistview1();
            refreshlistview2();
            lblStatus.Text = "Customer Removed";
        }

        ///**************************************************************************************************
        ///<summary> Event handler. Called by button7(Receipt) for click events.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<param name="sender"> [in] Source of the event.</param>
        ///<param name="e">      [in] Event information.</param>
        ///*************************************************************************************************
        private void button7_Click(object sender, EventArgs e)
        {
            FormReceipt f = new FormReceipt(cvm);
            f.ShowDialog();
        }

        ///**************************************************************************************************
        ///<summary> Event handler. Called by button1(process bill for table) for click events.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<param name="sender"> [in] Source of the event.</param>
        ///<param name="e">      [in] Event information.</param>
        ///*************************************************************************************************
        private void button1_Click(object sender, EventArgs e)
        {
            if (CustomerVM.TableCluster.FindIndex(x => x.TS.TableNum == TABLENUM
                                                && x.TS.SeatNum != -1) != -1)
            {
                if (MessageBox.Show("The Bill is Currently Distributed At other Customer(s)" +
                    "Are you sure you want to continue", "Warning",
                      MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No)
                    return;

                cvm.CreateAtTable();
                cvm.process_Bill();
                cvm.Bill.Clear();
                refreshlistview2();
                lblStatus.Text = "Table Bill Processed";
            }
            else
            {
                cvm.CreateAtTable();
                cvm.process_Bill();
                cvm.Bill.Clear();
                refreshlistview2();
                this.Hide();
                this.Close();
            }
        }

        ///**************************************************************************************************
        ///<summary> Event handler. Called by button2(process customer) for click events.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<param name="sender"> [in] Source of the event.</param>
        ///<param name="e">      [in] Event information.</param>
        ///*************************************************************************************************
        private void button2_Click(object sender, EventArgs e)
        {
            int seat = int.Parse(listView1.SelectedItems[0].SubItems[0].Text);

            CustomerVM cust = CustomerVM.TableCluster.Find(x => x.TS.TableNum == TABLENUM
                                                && x.TS.SeatNum == seat);

            cust.CreateAtTable();
            cust.process_Bill();
            refreshlistview1();
            lblStatus.Text = "Customer# " + cust.TS.SeatNum + " Processed";
        }
    }
}
