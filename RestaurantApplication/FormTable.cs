﻿using RestaurantVML;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace RestaurantApplication
{
    public partial class FormTable : Form
    {
        ///**************************************************************************************************
        ///<summary>Initializes a new instance of the RestaurantApplication.FormTable class.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<param name="tableNum"> [in] The table number.</param>
        ///*************************************************************************************************
        public FormTable(int tableNum)
        {
            TABLENUM = tableNum;
            InitializeComponent();
            InitializeComponent2();
        }

        ///**************************************************************************************************
        ///<summary> Refresh list view(table bill).</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///*************************************************************************************************
        private void refreshlistview2()
        {
            listView2.Items.Clear();
            foreach (ItemVM vm in cvm.Bill)
            {
                ListViewItem i = new ListViewItem("" + vm.Id);
                i.SubItems.Add(vm.Name);
                i.SubItems.Add("$" + vm.Price);
                this.listView2.Items.Add(i);
            }
            textBox1.Text = "$" + cvm.get_Bill_total();
        }

        ///**************************************************************************************************
        ///<summary> Event handler. Called by button7(add item(S)) for click events.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<param name="sender"> [in] Source of the event.</param>
        ///<param name="e">      [in] Event information.</param>
        ///*************************************************************************************************
        private void button7_Click(object sender, EventArgs e)
        {
            if (this.listView1.SelectedItems.Count == 0)
            {
                MessageBox.Show("An Item Must Be Selected to be Added", "Error",
                     MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                for (int index = 0; index < listView1.SelectedItems.Count; ++index)
                {
                    int id = int.Parse(listView1.SelectedItems[index].SubItems[0].Text);
                    ItemVM i = new ItemVM();
                    i.Id = id;
                    i.GetById();
                    cvm.Bill.Add(i);
                }
                refreshlistview2();
            }
        }

        ///**************************************************************************************************
        ///<summary> Event handler. Called by button1(remove item(S)) for click events.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<param name="sender"> [in] Source of the event.</param>
        ///<param name="e">      [in] Event information.</param>
        ///*************************************************************************************************
        private void button1_Click(object sender, EventArgs e)
        {
            if (this.listView2.SelectedItems.Count == 0)
            {
                MessageBox.Show("An Item Must Be Selected to be removed", "Error",
                     MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                int[] TBD = new int[listView2.SelectedItems.Count];
                for (int i = 0; i < TBD.Length; ++i)
                {
                    TBD[i] = int.Parse(listView2.SelectedItems[i].SubItems[0].Text);
                }
                for (int i = 0; i < TBD.Length; ++i)
                {
                    cvm.Bill.RemoveAt(cvm.Bill.FindIndex(x => x.Id == TBD[i]));
                }
                refreshlistview2();
            }
        }

        ///**************************************************************************************************
        ///<summary> Event handler. Called by button2(Manage bill) for click events.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<param name="sender"> [in] Source of the event.</param>
        ///<param name="e">      [in] Event information.</param>
        ///*************************************************************************************************
        private void button2_Click(object sender, EventArgs e)
        {
            if (cvm.Bill.Count != 0 && !CustomerVM.TableCluster.Exists(x=> x.TS.TableNum == TABLENUM))
            {
                cvm.TS = new CustomerVM.TSR(TABLENUM,-1);
                CustomerVM.TableCluster.Add(cvm);
            }
            if(cvm.Bill.Count == 0)
            {
                MessageBox.Show("An Item Must On the Table bill for it to be Managed", "Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            FormManageTableBill f = new FormManageTableBill(TABLENUM);
            f.ShowDialog();
            if (!f.Visible)
            {
                refreshlistview2();
            }
        }

        ///**************************************************************************************************
        ///<summary> Event handler. Called by button3(Ok) for click events.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<param name="sender"> [in] Source of the event.</param>
        ///<param name="e">      [in] Event information.</param>
        ///*************************************************************************************************
        private void button3_Click(object sender, EventArgs e)
        {
            if (cvm.Bill.Count != 0 && !CustomerVM.TableCluster.Exists(x => x.TS.TableNum == TABLENUM))
            {
                cvm.TS = new CustomerVM.TSR(TABLENUM,-1);
                CustomerVM.TableCluster.Add(cvm);
            }
            if(cvm.Bill.Count == 0 && CustomerVM.TableCluster.Exists(x => x.TS.TableNum == TABLENUM))
            {
                CustomerVM.TableCluster.RemoveAt(
                    CustomerVM.TableCluster.FindIndex((x => x.TS.TableNum == TABLENUM)));
            }
            this.Hide();
            this.Close();
        }
    }
}
