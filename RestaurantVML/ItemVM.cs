﻿///**************************************************************************************************
/// file:	ItemVM.cs
/// <author>Joshua Wright</author>
/// <date>2016-11-06</date>
///*************************************************************************************************
using System.Collections.Generic;
using RestaurantDAL;

namespace RestaurantVML
{
    ///**************************************************************************************************
    ///<summary> An item view model.</summary>
    ///<remarks> Joshua Wright, 2016-11-06.</remarks>
    ///*************************************************************************************************
    public class ItemVM
    {
        private ItemDAO dao_;
        public int Id { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public int Version { get; set; }

        public ItemVM()
        {
            dao_ = new ItemDAO();
        }

        public ItemVM(int id,string name,double price,int version)
        {
            Id = id;
            Name = name;
            Price = price;
            Version = version;
        }

        ///**************************************************************************************************
        ///<summary> Creates An Item.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///*************************************************************************************************
        public void Create()
        {
            Item item = new Item();
            item.Name = Name;
            item.Price = Price;
            dao_.Create(item);
            Id = item.Id;
            Version = item.Version;
        }

        ///**************************************************************************************************
        ///<summary> Deletes this Item.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<returns> An int.</returns>
        ///*************************************************************************************************
        public int delete()
        {
            return dao_.Delete(Id);
        }

        ///**************************************************************************************************
        ///<summary> Gets Item by identifier.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///*************************************************************************************************
        public void GetById()
        {
            Item item = dao_.GetById(Id);
            Name = item.Name;
            Price = item.Price;
            Version = item.Version;
        }

        ///**************************************************************************************************
        ///<summary> Gets all Items.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<returns> all.</returns>
        ///*************************************************************************************************
        public List<ItemVM> GetAll()
        {
            List<ItemVM> result = new List<ItemVM>();

            List<Item> list = dao_.GetAll();

            foreach(Item i in list)
            {
                result.Add(new ItemVM(i.Id,i.Name,i.Price, i.Version));
            }

            return result;
        }

        ///**************************************************************************************************
        ///<summary> Updates this Item.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///*************************************************************************************************
        public void Update()
        {
            Item item = new Item();
            item.Name = Name;
            item.Price = Price;
            item.Id = Id;
            dao_.Update(item);
            Version = item.Version;
        }

        ///**************************************************************************************************
        ///<summary> Converts this ItemVM to a string.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<returns> This ItemVM as a string.</returns>
        ///*************************************************************************************************
        public string to_string()
        {
            return Name + ":  $" + Price;
        }
    }
}
