﻿///**************************************************************************************************
/// file:	EmployeeVM.cs
/// <author>Joshua Wright</author>
/// <date>2016-11-06</date>
///*************************************************************************************************
using RestaurantDAL;
using System.Collections.Generic;

namespace RestaurantVML
{
    ///**************************************************************************************************
    ///<summary> An Employee view model.</summary>
    ///<remarks> Joshua Wright, 2016-11-06.</remarks>
    ///*************************************************************************************************
    public class EmployeeVM
    {
        private EmployeeDAO dao_;

        public int Id { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public int Version { get; set; }
        public double PayRate { get; set; }
        public int HoursWorked { get; set; }
        public int AddressId { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }


        public EmployeeVM()
        {
            dao_ = new EmployeeDAO();
        }

        ///**************************************************************************************************
        ///<summary> Gets Employee by identifier.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///*************************************************************************************************
        public void GetById()
        {
            Employee emp = dao_.GetById(Id);
            Firstname = emp.Firstname;
            Lastname = emp.Lastname;
            Phone = emp.Phone;
            Email = emp.Email;
            PayRate = emp.PayRate;
            HoursWorked = emp.HoursWorked;
            Id = emp.Id;
            AddressId = AddressId;
            Version = emp.Version;
            Street = emp.Street;
            City = emp.City;
            PostalCode = emp.PostalCode;
            Country = emp.Country;
        }

        ///**************************************************************************************************
        ///<summary> Deletes this Employee.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///*************************************************************************************************
        public void Delete()
        {
            dao_.Delete(Id);
        }

        ///**************************************************************************************************
        ///<summary> Gets all Employees.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<returns> all.</returns>
        ///*************************************************************************************************
        public List<EmployeeVM> GetAll()
        {
            List<EmployeeVM> list = new List<EmployeeVM>();
            List<Employee> result = dao_.GetAll();

            foreach (Employee emp in result)
            {
                EmployeeVM evm = new EmployeeVM();
                evm.Firstname = emp.Firstname;
                evm.Lastname = emp.Lastname;
                evm.Phone = emp.Phone;
                evm.Email = emp.Email;
                evm.PayRate = emp.PayRate;
                evm.HoursWorked = emp.HoursWorked;
                evm.Id = emp.Id;
                evm.AddressId = AddressId;
                evm.Version = emp.Version;
                evm.Street = emp.Street;
                evm.City = emp.City;
                evm.PostalCode = emp.PostalCode;
                evm.Country = emp.Country;
                list.Add(evm);
            }
            return list;
        }

        ///**************************************************************************************************
        ///<summary> Creates An Employee.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///*************************************************************************************************
        public void Create()
        {
            Employee emp = new Employee();
            emp.Firstname = Firstname;
            emp.Lastname = Lastname;
            emp.Phone = Phone;
            emp.Email = Email;
            emp.PayRate = PayRate;
            emp.HoursWorked = HoursWorked;
            emp.Street = Street;
            emp.City = City;
            emp.PostalCode = PostalCode;
            emp.Country = Country;
            dao_.Create(emp);
            Id = emp.Id;
            Version = emp.Version;
        }

        ///**************************************************************************************************
        ///<summary> Updates this EmployeeVM.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///*************************************************************************************************
        public void Update()
        {
            Employee emp = new Employee();
            emp.Id = Id;
            emp.Firstname = Firstname;
            emp.Lastname = Lastname;
            emp.Phone = Phone;
            emp.Email = Email;
            emp.PayRate = PayRate;
            emp.HoursWorked = HoursWorked;
            emp.Street = Street;
            emp.City = City;
            emp.PostalCode = PostalCode;
            emp.Country = Country;
            dao_.Update(emp);
            Version = emp.Version;
        }
    }
}
