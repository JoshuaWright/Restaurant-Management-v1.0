﻿///**************************************************************************************************
/// file:	CustomerVM.cs
/// <author>Joshua Wright</author>
/// <date>2016-11-06</date>
///*************************************************************************************************
using System.Collections.Generic;
using RestaurantDAL;
using System;
using System.Text;

namespace RestaurantVML
{
    ///**************************************************************************************************
    ///<summary> A customer view Model.</summary>
    ///<remarks> Joshua Wright, 2016-11-06.</remarks>
    ///*************************************************************************************************
    public class CustomerVM
    {
        private CustomerDAO dao_;

        ///**************************************************************************************************
        ///<summary> Used to Hold table Number and Seat Number.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///*************************************************************************************************
        public struct TSR
        {
            public TSR(int tableNum, int seatNum)
            {
                TableNum = tableNum;
                SeatNum = seatNum;
            }

            public int TableNum { get; set; }
            public int SeatNum { get; set; }
        };


        public int Id { get; set; }
        public TSR TS { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public int Version { get; set; }
        public int AddressId { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public List<ItemVM> Bill { get; set; }

        ///**************************************************************************************************
        ///<summary> Records the tables and seats being used by ALL customers.</summary>
        ///<value> The table seat record.</value>
        ///*************************************************************************************************
        public static List<TSR> TableSeatRecord { get; set; } = new List<TSR>();

        ///**************************************************************************************************
        ///<summary> Used to hold Customers AS a table.</summary>
        ///<value> The table cluster.</value>
        ///*************************************************************************************************
        public static List<CustomerVM> TableCluster { get; set; } = new List<CustomerVM>();
       
        public CustomerVM()
        {
            dao_ = new CustomerDAO();
            Bill = new List<ItemVM>();
            TS = new TSR();
        }

        public CustomerVM(int id, string fname, string lname, 
            string phone, string email,List<ItemVM> bill = null)
        {
            Id = id;
            Firstname = fname;
            Lastname = lname;
            Phone = phone;
            Email = email;
            dao_ = new CustomerDAO();
            Bill = new List<ItemVM>();
            TS = new TSR();
            Bill = bill;
        }

        ///**************************************************************************************************
        ///<summary> Gets by identifier A full inComplete CustomerVM(NOT PAID).</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///*************************************************************************************************
        public void GetByIdFullInComplete()
        {
            Customer cust = dao_.GetByIdFullInComplete(Id);
            Firstname = cust.Firstname;
            Lastname = cust.Lastname;
            Phone = cust.Phone;
            Email = cust.Email;
            Street = cust.Street;
            PostalCode = cust.PostalCode;
            Country = cust.Country;
            City = cust.City;
            Id = cust.Id;
            Version = cust.Version;
        }

        ///**************************************************************************************************
        ///<summary> Creates full incomplete CustomerVM(NOT PAID).</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///*************************************************************************************************
        public void CreateFullInComplete()
        {
            Customer cust = new Customer();
            cust.Firstname = Firstname;
            cust.Lastname = Lastname;
            cust.Phone = Phone;
            cust.Email = Email;
            cust.Street = Street;
            cust.City = City;
            cust.Country = Country;
            cust.PostalCode = PostalCode;
            cust = dao_.CreateFull(cust);
            Id = cust.Id;
            process_BillInComplete();
        }

        ///**************************************************************************************************
        ///<summary> Creates a CustomerVM seated at a table.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///*************************************************************************************************
        public void CreateAtTable()
        {
            Customer cust = new Customer();
            cust.Firstname = "";
            cust.Lastname = "";
            cust.Phone = "";
            cust.Email = "";
            cust.TableNum = TS.TableNum;
            dao_.Create(cust);
            Id = cust.Id;
        }

        ///**************************************************************************************************
        ///<summary> Deletes this CustomerVM.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///*************************************************************************************************
        public void Delete()
        {
            dao_.Delete(Id);
        }

        ///**************************************************************************************************
        ///<summary> Gets all full incomplete CustomerVM(NOT PAID).</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<returns> all full in complete.</returns>
        ///*************************************************************************************************
        public List<CustomerVM> GetAllFullInComplete()
        {
            List<CustomerVM> list = new List<CustomerVM>();
            List<Customer> result = dao_.GetAllFullInComplete();

            foreach (Customer cust in result)
            {
                CustomerVM vm = new CustomerVM(cust.Id, cust.Firstname,
                    cust.Lastname, cust.Phone, cust.Email);
                vm.Street = cust.Street;
                vm.City = cust.City;
                vm.PostalCode = cust.PostalCode;
                vm.Country = cust.Country;
                list.Add(vm);
            }
            return list;
        }

        ///**************************************************************************************************
        ///<summary> Process the Customers bill(PAID).</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///*************************************************************************************************
        public void process_Bill()
        {
           foreach(ItemVM i in Bill)
            {
                dao_.add_Item(Id, i.Id);
            }
        }

        ///**************************************************************************************************
        ///<summary> Process the Customers bill incomplete(NOT PAID).</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///*************************************************************************************************
        public void process_BillInComplete()
        {
            foreach (ItemVM i in Bill)
            {
                dao_.add_ItemInComplete(Id, i.Id);
            }
        }

        ///**************************************************************************************************
        ///<summary> Pay CustomerInComplete bill.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///*************************************************************************************************
        public void pay_bill()
        {
            dao_.pay_Bill(Id);
        }

        ///**************************************************************************************************
        ///<summary> Gets bill total.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<returns> The bill total.</returns>
        ///*************************************************************************************************
        public double get_Bill_total()
        {
            double total = 0;
            foreach (ItemVM i in Bill)
            {
                total += i.Price;
            }
            return total;
        }

        ///**************************************************************************************************
        ///<summary> Gets the bill.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///*************************************************************************************************
        public void GetBill()
        {
            List<Item> list = dao_.GetBill(Id);

            foreach (Item itm in  list)
            {
                ItemVM i = new ItemVM()
                {
                    Name = itm.Name,
                    Price = itm.Price,
                    Id = itm.Id
                };
                Bill.Add(i);
            }
        }

        ///**************************************************************************************************
        ///<summary> Creates A receipt.</summary>
        ///<remarks> Joshua Wright, 2016-11-06.</remarks>
        ///<returns> A string.</returns>
        ///*************************************************************************************************
        public string Receipt()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Company Name");
            sb.AppendLine("123 St");
            sb.AppendLine("City , Province");
            sb.AppendLine("Date: " + DateTime.Now.ToString());
            sb.AppendLine("");
            sb.AppendLine("Bill:");
            foreach(ItemVM itm in Bill)
            {
                sb.AppendLine(itm.to_string());
            }
            sb.AppendLine("");
            sb.AppendLine("*********************");
            sb.AppendLine("Sub-Total:  $" + get_Bill_total());
            sb.AppendLine("Tax:  $0");
            sb.AppendLine("Total:  $" + get_Bill_total());
            return sb.ToString();
        }

    }
}

